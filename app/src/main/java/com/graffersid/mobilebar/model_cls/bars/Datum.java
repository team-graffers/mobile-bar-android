package com.graffersid.mobilebar.model_cls.bars;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 04/10/2019 at 03:02 PM.
 */
public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bottle_id")
    @Expose
    private Integer bottleId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("purchased_bottle")
    @Expose
    private Integer purchasedBottle;
    @SerializedName("expiry")
    @Expose
    private String expiry;
    @SerializedName("remaining_ml")
    @Expose
    private Integer remainingMl;
    @SerializedName("total_ml")
    @Expose
    private Integer totalMl;
    @SerializedName("bar")
    @Expose
    private List<Bar> bar = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBottleId() {
        return bottleId;
    }

    public void setBottleId(Integer bottleId) {
        this.bottleId = bottleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getPurchasedBottle() {
        return purchasedBottle;
    }

    public void setPurchasedBottle(Integer purchasedBottle) {
        this.purchasedBottle = purchasedBottle;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public Integer getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Integer remainingMl) {
        this.remainingMl = remainingMl;
    }

    public Integer getTotalMl() {
        return totalMl;
    }

    public void setTotalMl(Integer totalMl) {
        this.totalMl = totalMl;
    }

    public List<Bar> getBar() {
        return bar;
    }

    public void setBar(List<Bar> bar) {
        this.bar = bar;
    }

}
