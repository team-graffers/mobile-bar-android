package com.graffersid.mobilebar.model_cls.my_bar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandy on 16/09/2019 at 05:57 PM.
 */
public class OrderdrinkCustomer {

    @SerializedName("bar_id")
    @Expose
    private Integer barId;
    @SerializedName("bar_name")
    @Expose
    private String barName;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("consumed_ml")
    @Expose
    private Double consumedMl;

    public Integer getBarId() {
        return barId;
    }

    public void setBarId(Integer barId) {
        this.barId = barId;
    }

    public String getBarName() {
        return barName;
    }

    public void setBarName(String barName) {
        this.barName = barName;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Double getConsumedMl() {
        return consumedMl;
    }

    public void setConsumedMl(Double consumedMl) {
        this.consumedMl = consumedMl;
    }
}
