package com.graffersid.mobilebar.model_cls.my_bar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandy on 09/09/2019 at 11:05 AM.
 */
public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bar_name")
    @Expose
    private String barName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("bottle_count")
    @Expose
    private Integer bottleCount;
    @SerializedName("customerbottle_count")
    @Expose
    private Integer customerbottleCount;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bar")
    @Expose
    private List<Integer> bar = null;
    @SerializedName("purchased_bottle")
    @Expose
    private Integer purchasedBottle;
    @SerializedName("updated_date_time")
    @Expose
    private String updatedDateTime;
    @SerializedName("expiry")
    @Expose
    private String expiry;
    @SerializedName("remaining_ml")
    @Expose
    private Integer remainingMl;
    @SerializedName("total_ml_buy")
    @Expose
    private Integer totalMlBuy;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("bar_id")
    @Expose
    private Integer barId;
    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("consumed_ml")
    @Expose
    private Double consumedMl;
    @SerializedName("orderdrink_customer")
    @Expose
    private List<OrderdrinkCustomer> orderdrinkCustomer = null;
    @SerializedName("create-date")
    @Expose
    private String createDate;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("customer_by_id")
    @Expose
    private String customerById;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("is_accepted")
    @Expose
    private Boolean isAccepted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBarName() {
        return barName;
    }

    public void setBarName(String barName) {
        this.barName = barName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getBottleCount() {
        return bottleCount;
    }

    public void setBottleCount(Integer bottleCount) {
        this.bottleCount = bottleCount;
    }

    public Integer getCustomerbottleCount() {
        return customerbottleCount;
    }

    public void setCustomerbottleCount(Integer customerbottleCount) {
        this.customerbottleCount = customerbottleCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getBar() {
        return bar;
    }

    public void setBar(List<Integer> bar) {
        this.bar = bar;
    }

    public Integer getPurchasedBottle() {
        return purchasedBottle;
    }

    public void setPurchasedBottle(Integer purchasedBottle) {
        this.purchasedBottle = purchasedBottle;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public Integer getRemainingMl() {
        return remainingMl;
    }

    public void setRemainingMl(Integer remainingMl) {
        this.remainingMl = remainingMl;
    }

    public Integer getTotalMlBuy() {
        return totalMlBuy;
    }

    public void setTotalMlBuy(Integer totalMlBuy) {
        this.totalMlBuy = totalMlBuy;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getBarId() {
        return barId;
    }

    public void setBarId(Integer barId) {
        this.barId = barId;
    }

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Double getConsumedMl() {
        return consumedMl;
    }

    public void setConsumedMl(Double consumedMl) {
        this.consumedMl = consumedMl;
    }

    public List<OrderdrinkCustomer> getOrderdrinkCustomer() {
        return orderdrinkCustomer;
    }

    public void setOrderdrinkCustomer(List<OrderdrinkCustomer> orderdrinkCustomer) {
        this.orderdrinkCustomer = orderdrinkCustomer;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCustomerById() {
        return customerById;
    }

    public void setCustomerById(String customerById) {
        this.customerById = customerById;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }
}
