package com.graffersid.mobilebar.model_cls.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
* Created by Sandy on 07/10/2019 at 11:31 AM.
*/
public class Purchased {

    @SerializedName("bottle_name")
    @Expose
    private String bottleName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("is_renew")
    @Expose
    private Boolean isRenew;

    public String getBottleName() {
        return bottleName;
    }

    public void setBottleName(String bottleName) {
        this.bottleName = bottleName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(Boolean isRenew) {
        this.isRenew = isRenew;
    }

}
