package com.graffersid.mobilebar.frags;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.TransactionHistoryAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.graffersid.mobilebar.model_cls.transaction.HistoryTrans;
import com.graffersid.mobilebar.model_cls.transaction.Transaction;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionHistoryFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    TransactionHistoryAdapter adapter;

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    private static String nextPage, previousPage;

    private List<HistoryTrans> listHistory;
    private HashMap<String, String> headers;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private TextView noHistory;

    Context context;

    public TransactionHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transaction_history, container, false);

        context = getActivity();

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_transaction_history);
        noHistory = (TextView) view.findViewById(R.id.no_history);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        Universal_Var_Cls.loader(context);

        listHistory = new ArrayList<>();
        adapter = new TransactionHistoryAdapter(context, listHistory);

        noHistory.setVisibility(View.GONE);

        layoutManager = new GridLayoutManager(context, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        historyList(Universal_Var_Cls.transactionHistory);
    }


    private void historyList(String url) {

        if (Universal_Var_Cls.dialog.isShowing()) {

        } else {
            Universal_Var_Cls.dialog.show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_transaction", "S   " + response.toString());

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                Transaction transaction = new Gson().fromJson(response.toString(), Transaction.class);

                if (transaction.getPurchased().size() > 0) {

                    HistoryTrans historyTrans;

                    for (int i = 0; i < transaction.getPurchased().size(); i++) {

                        historyTrans = new HistoryTrans();

                        if (transaction.getPurchased().get(i).getIsRenew()) {
                            historyTrans.setStatus(2);
                        } else {
                            historyTrans.setStatus(1);
                        }
                        historyTrans.setBottleName(transaction.getPurchased().get(i).getBottleName());
                        historyTrans.setImage(transaction.getPurchased().get(i).getImage());
                        historyTrans.setDate(transaction.getPurchased().get(i).getDate());
                        historyTrans.setPrice(transaction.getPurchased().get(i).getPrice());

                        listHistory.add(historyTrans);
                    }
                }
                if (transaction.getExtend().size() > 0) {
                    HistoryTrans historyTrans;

                    for (int i = 0; i < transaction.getExtend().size(); i++) {

                        historyTrans = new HistoryTrans();

                        historyTrans.setStatus(3);

                        historyTrans.setBottleName(transaction.getExtend().get(i).getBottleName());
                        historyTrans.setImage(transaction.getExtend().get(i).getImage());
                        historyTrans.setDate(transaction.getExtend().get(i).getDate());
                        historyTrans.setPrice(transaction.getExtend().get(i).getPrice());
                        historyTrans.setStartDate(transaction.getExtend().get(i).getStartDate());
                        historyTrans.setEndDate(transaction.getExtend().get(i).getEndDate());

                        listHistory.add(historyTrans);
                    }
                }
                if (transaction.getReffer().size() > 0) {
                    HistoryTrans historyTrans;

                    for (int i = 0; i < transaction.getReffer().size(); i++) {

                        historyTrans = new HistoryTrans();

                        historyTrans.setStatus(4);

                        historyTrans.setBottleName("Refer");
                        historyTrans.setName(transaction.getReffer().get(i).getName());
                        historyTrans.setDate(transaction.getReffer().get(i).getDate());
                        historyTrans.setPoint(transaction.getReffer().get(i).getPoint());

                        listHistory.add(historyTrans);
                    }
                }

                Comparator<HistoryTrans> compareById = new Comparator<HistoryTrans>() {
                    @Override
                    public int compare(HistoryTrans o1, HistoryTrans o2) {
                        return o1.getDate().compareTo(o2.getDate());
                    }
                };

                Collections.sort(listHistory, compareById);


                adapter.notifyDataSetChanged();

                if (transaction.getPurchased().size() > 0 || transaction.getExtend().size() > 0 || transaction.getReffer().size() > 0) {
                    noHistory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }else {
                    noHistory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("resp_transaction", "E   " + error.toString());

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
