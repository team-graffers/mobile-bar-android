package com.graffersid.mobilebar.frags;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.BubbleThumbRangeSeekbar;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterPriceRangeFragment extends Fragment {

    TextView minPrice, maxPrice, minimumPrice, maximumPrice;

    public FilterPriceRangeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_price_range, container, false);

        minPrice = (TextView) view.findViewById(R.id.min_price_filter);
        maxPrice = (TextView) view.findViewById(R.id.max_price_filter);

        minimumPrice = (TextView) view.findViewById(R.id.minimum_price_filter);
        maximumPrice = (TextView) view.findViewById(R.id.maximum_price_filter);

        BubbleThumbRangeSeekbar bubbleThumbRangeSeekbar = (BubbleThumbRangeSeekbar) view.findViewById(R.id.rangeSeekbar5);
        bubbleThumbRangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minPrice.setText("S$ "+minValue);
                maxPrice.setText("S$ "+maxValue);
                Universal_Var_Cls.minSelectedFilterPrice = Integer.parseInt(String.valueOf(minValue));
                Universal_Var_Cls.maxSelectedFilterPrice = Integer.parseInt(String.valueOf(maxValue));

                Log.d("wwwwwwwwwww", ""+Universal_Var_Cls.minSelectedFilterPrice);
            }
        });
        bubbleThumbRangeSeekbar.setMinValue(Universal_Var_Cls.minPriceFilter);
        bubbleThumbRangeSeekbar.setMaxValue(Universal_Var_Cls.maxPriceFilter);
        if (Universal_Var_Cls.minSelectedFilterPrice > 0){
            bubbleThumbRangeSeekbar.setMinStartValue(10);
        }else {
            bubbleThumbRangeSeekbar.setMinStartValue(10);
        }
        if (Universal_Var_Cls.maxSelectedFilterPrice > 0){
            bubbleThumbRangeSeekbar.setMaxStartValue(Universal_Var_Cls.maxSelectedFilterPrice);
        }else {
            bubbleThumbRangeSeekbar.setMaxStartValue(Universal_Var_Cls.maxPriceFilter);
        }

        minimumPrice.setText("S$ "+Universal_Var_Cls.minPriceFilter);
        maximumPrice.setText("S$ "+Universal_Var_Cls.maxPriceFilter);
        minPrice.setText("S$ "+bubbleThumbRangeSeekbar.getSelectedMinValue());
        int maxSelected = Integer.parseInt(String.valueOf(bubbleThumbRangeSeekbar.getSelectedMaxValue())) - 1;
        maxPrice.setText("S$ "+maxSelected);

        /*Universal_Var_Cls.minSelectedFilterPrice = Integer.parseInt(String.valueOf(bubbleThumbRangeSeekbar.getSelectedMinValue()));
        Universal_Var_Cls.maxSelectedFilterPrice = maxSelected;*/

        return view;
    }

}
