package com.graffersid.mobilebar.frags;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.BottleHistoryAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottleHistoryFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    HashMap<String, String> headers;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    Context context;
    List<Result> listHistoryBottle;
    BottleHistoryAdapter adapter;
    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    private static String nextPage, previousPage;
    private TextView noHistory;

    public BottleHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bottle_history, container, false);

        context = getActivity();

        Universal_Var_Cls.loader(context);

        nextPage = null;

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_bottle_history);
        noHistory = (TextView) view.findViewById(R.id.no_history);

        noHistory.setVisibility(View.GONE);

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");


        listHistoryBottle = new ArrayList<>();
        adapter = new BottleHistoryAdapter(context, listHistoryBottle);


        Universal_Var_Cls.dialog.show();

        historyList(Universal_Var_Cls.bottleHistory);



        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                historyList(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
        return view;
    }

    private void historyList(String url) {


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_bottle_his_s", response.toString());


                if (Universal_Var_Cls.dialog.isShowing()){
                    Universal_Var_Cls.dialog.dismiss();
                }
                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getNext() != null){

                    String str = myBarModelCls.getNext();

                    if (str.contains("localhost")){
                        str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                    }
                    nextPage = str;
                }else {
                    nextPage = null;
                }

                if (myBarModelCls.getResults() != null && !myBarModelCls.getResults().isEmpty()){

                    noHistory.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    listHistoryBottle.addAll(myBarModelCls.getResults());

                    adapter.notifyDataSetChanged();

                    layoutManager = new GridLayoutManager(context, 1);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                }else {
                    noHistory.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (Universal_Var_Cls.dialog.isShowing()){
                    Universal_Var_Cls.dialog.dismiss();
                }
                Log.d("resp_bottle_his_e", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
