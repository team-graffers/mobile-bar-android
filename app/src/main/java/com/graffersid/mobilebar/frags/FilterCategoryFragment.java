package com.graffersid.mobilebar.frags;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.FilterCategoryAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.filter_category_list.FilterCategoryList;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.graffersid.mobilebar.classes.Universal_Var_Cls.listcategory;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterCategoryFragment extends Fragment {

    RecyclerView recyclerView;
    Context context;
    public static FilterCategoryAdapter adapter;
    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;

    public FilterCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_category, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.category_filter_recycler_vw);
        context = getActivity();

        if (Universal_Var_Cls.filterCategoryList.getCount() > 0){prepareCategoryList();}
        return view;
    }

    private void prepareCategoryList() {

        listcategory = new ArrayList<>();

        layoutManager = new GridLayoutManager(context, 3);
        adapter = new FilterCategoryAdapter(context, listcategory);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        ResultCategory resultCategory;

        for (int i=0; i<Universal_Var_Cls.filterCategoryList.getResults().size(); i++){

            resultCategory = new ResultCategory();
            resultCategory.setId(Universal_Var_Cls.filterCategoryList.getResults().get(i).getId());
            resultCategory.setImage(Universal_Var_Cls.filterCategoryList.getResults().get(i).getImage());
            resultCategory.setName(Universal_Var_Cls.filterCategoryList.getResults().get(i).getName());

            if (!Universal_Var_Cls.filterCategoryIdList.isEmpty() && Universal_Var_Cls.filterCategoryIdList.size() > i){
                if (Universal_Var_Cls.filterCategoryIdList.get(i).equals("-1")){
                    resultCategory.setIsSelect(false);
                }else {
                    resultCategory.setIsSelect(true);
                }
            }else {
                resultCategory.setIsSelect(false);
                Universal_Var_Cls.filterCategoryIdList.add("-1");
            }
            listcategory.add(resultCategory);
            adapter.notifyDataSetChanged();
        }

        nextPage = Universal_Var_Cls.filterCategoryList.getNext();

        if (nextPage != null){

                String str = nextPage;

                if (str.contains("localhost")){
                    str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                }
                nextPage = str;
            categoryListData(nextPage);
        }
        paginationMethod();
    }

    private void paginationMethod() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                categoryListData(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void categoryListData(final String url) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                FilterCategoryList filterCategoryList = new Gson().fromJson(response.toString(), FilterCategoryList.class);

                nextPage = filterCategoryList.getNext();

                ResultCategory resultCategory;
                for (int i=0; i<filterCategoryList.getResults().size(); i++){

                    resultCategory = new ResultCategory();
                    resultCategory.setId(filterCategoryList.getResults().get(i).getId());
                    resultCategory.setName(filterCategoryList.getResults().get(i).getName());
                    resultCategory.setImage(filterCategoryList.getResults().get(i).getImage());

                    boolean idCat = false;
                    int k;
                    for (k=0; k<Universal_Var_Cls.filterCategoryIdList.size(); k++){
                        if (String.valueOf(filterCategoryList.getResults().get(i).getId()).equals(Universal_Var_Cls.filterCategoryIdList.get(k))){
                            idCat = true;
                            break;
                        }
                    }
                    if (idCat){
                        resultCategory.setIsSelect(true);
                    }else {
                        resultCategory.setIsSelect(false);
                        if (Universal_Var_Cls.filterCategoryIdList.size() <= i){
                            Universal_Var_Cls.filterCategoryIdList.add("-1");
                        }
                    }

                    listcategory.add(resultCategory);
                    adapter.notifyDataSetChanged();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

}
