package com.graffersid.mobilebar.frags;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.ConsumeHistoryAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.consume_history.ConsumeHistoryModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsumeHistoryFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    HashMap<String, String> headers;
    ConsumeHistoryAdapter adapter;
    Context context;
    private LinearLayout layoutHistory;
    private TextView noHistory;

    public ConsumeHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_consume_history, container, false);

        context = getActivity();

        Universal_Var_Cls.loader(context);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_vw_consume);
        noHistory = (TextView) view.findViewById(R.id.no_history);
        layoutHistory = (LinearLayout) view.findViewById(R.id.history_found);

        recyclerView.setPadding(0, 10, 0, 40);

        preferences = getActivity().getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        historyList();
        return view;
    }

    private void historyList() {

        Universal_Var_Cls.dialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Universal_Var_Cls.consumptionHistory, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                ConsumeHistoryModel consumeHistoryModel = new Gson().fromJson(response.toString(), ConsumeHistoryModel.class);

                if (!consumeHistoryModel.getData().isEmpty()){
                    adapter = new ConsumeHistoryAdapter(getActivity(), consumeHistoryModel.getData());

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);

                    noHistory.setVisibility(View.GONE);
                    layoutHistory.setVisibility(View.VISIBLE);
                }else {
                    noHistory.setVisibility(View.VISIBLE);
                    layoutHistory.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

}
