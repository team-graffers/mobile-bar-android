package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.BarStoriesActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyBarByBarAdapter extends RecyclerView.Adapter<MyBarByBarAdapter.VwHolder> {

    Context context;
    List<Result> listBar;
    LayoutInflater inflater;

    public MyBarByBarAdapter(Context context, List<Result> listBar) {
        this.context = context;
        this.listBar = listBar;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_by_bar_bottle_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        holder.nameBar.setText(listBar.get(position).getBarName());

        if (listBar.get(position).getDistance() != null && listBar.get(position).getDistance() != "null" && listBar.get(position).getDistance() != "") {

            double distance = Double.parseDouble(listBar.get(position).getDistance());
            holder.distanceBar.setText(String.format("%.2f", distance) + " km away");
        }
        holder.noOfBottle.setText(""+listBar.get(position).getCustomerbottleCount()+" Bottles");
        String consumedBottle = "Consume <font color = #EA2E2E>"+listBar.get(position).getCustomerbottleCount()+"/"+listBar.get(position).getBottleCount()+"</font> Purchased Bottle";
        holder.consumedLiqour.setText(Html.fromHtml(consumedBottle));

        if (listBar.get(position).getImage() != null) {
            Picasso.with(context).load(Universal_Var_Cls.baseUrl + "/" + listBar.get(position).getImage()).placeholder(R.drawable.bottle_detail_header).error(R.drawable.bottle_detail_header).into(holder.imgBar);
        }

        holder.noOfBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Universal_Var_Cls.barId = listBar.get(position).getId();
                context.startActivity(new Intent(context, BarStoriesActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBar.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imgBar;
        TextView nameBar, distanceBar, consumedLiqour;
        Button noOfBottle;
        public VwHolder(@NonNull View itemView) {
            super(itemView);

            imgBar = (ImageView) itemView.findViewById(R.id.img_bar);
            nameBar = (TextView) itemView.findViewById(R.id.bar_name);
            distanceBar = (TextView) itemView.findViewById(R.id.bar_distance);
            consumedLiqour = (TextView) itemView.findViewById(R.id.consumed_liqour);
            noOfBottle = (Button) itemView.findViewById(R.id.no_of_bottle);
        }
    }
}
