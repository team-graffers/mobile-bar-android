package com.graffersid.mobilebar.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.BarListInByBottleActivity;
import com.graffersid.mobilebar.activities.ExtendValidityActivity;
import com.graffersid.mobilebar.activities.GiftActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MyBarByBottleAdapter extends RecyclerView.Adapter<MyBarByBottleAdapter.VwHolder> {

    private final WindowManager.LayoutParams lWindowParams;

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private final DisplayMetrics displayMetrics;
    private int height;
    private int width;

    private Context context;
    List<Result> listBottle;

    Drawable wrappedDrawable;
    static LinearLayout layoutQtyBar, layoutColorBar;
    static RelativeLayout layoutQtyBack;
    static TextView txtQtyMargin, txtQty, txtColorMargin, txtColor;
    Dialog dialog;
    HashMap<String, String> headers;

    public MyBarByBottleAdapter(Context context, List<Result> listBottle) {

        this.context = context;
        this.listBottle = listBottle;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_bottle_gift_by_bottle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        displayMetrics = new DisplayMetrics();

        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);


        height = Math.round(displayMetrics.heightPixels);

        height = height - (height * 10 / 100);

        width = Math.round(displayMetrics.widthPixels);

        width = width - (width * 10 / 100);

        lWindowParams = new WindowManager.LayoutParams();

//        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
        lWindowParams.width = width;/*WindowManager.LayoutParams.FILL_PARENT;*/ // this is where the magic happens
        lWindowParams.height = height;/*WindowManager.LayoutParams.WRAP_CONTENT;*/

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();


        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.back_liquor_qty);
        wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VwHolder(LayoutInflater.from(context).inflate(R.layout.item_by_bottle, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        float remain;
        float sum;
        if (listBottle.get(position).getTotalMlBuy() != null) {
            if (listBottle.get(position).getRemainingMl() != null) {

                sum = (float) listBottle.get(position).getTotalMlBuy();
                remain = (float) listBottle.get(position).getRemainingMl();
                layoutQtyBar.setWeightSum(sum);
                layoutColorBar.setWeightSum(sum);
                setColorMethod(remain, sum);
            }
        }

        holder.brandName.setText(listBottle.get(position).getName());
        holder.noOfBottle.setText("No. of Bottle: " + listBottle.get(position).getPurchasedBottle());
        holder.btnNoOfBar.setText("" + listBottle.get(position).getBar().size() + " Bar");

        if (listBottle.get(position).getImage() != null) {
            Picasso.with(context).load(Universal_Var_Cls.baseUrl + listBottle.get(position).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.imgBottle);
        }else {
            holder.imgBottle.setImageResource(R.drawable.bottle_gray);
        }

        holder.btnExtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                extendValidityMethod(position);
            }
        });
        holder.btnGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                giftBottleMethod(position);
            }
        });

        holder.imgBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listBottleMethod(position);
            }
        });
        holder.btnNoOfBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Universal_Var_Cls.bottleId = listBottle.get(position).getId();
                context.startActivity(new Intent(context, BarListInByBottleActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBottle.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        TextView brandName, noOfBottle;
        ImageView imgBottle;
        Button btnExtValidity, btnNoOfBar;
        ImageButton btnGift;

        public VwHolder(@NonNull View itemView) {
            super(itemView);


            layoutQtyBar = (LinearLayout) itemView.findViewById(R.id.liqour_qty_bar);
            layoutQtyBack = (RelativeLayout) itemView.findViewById(R.id.liqour_qty_back);
            txtQtyMargin = (TextView) itemView.findViewById(R.id.liqour_qty_margin);
            txtQty = (TextView) itemView.findViewById(R.id.liqour_qty);

            layoutColorBar = (LinearLayout) itemView.findViewById(R.id.liqour_bar);
            txtColorMargin = (TextView) itemView.findViewById(R.id.liqour_margin_color_bar);
            txtColor = (TextView) itemView.findViewById(R.id.liqour_color_bar);


            brandName = (TextView) itemView.findViewById(R.id.brand_name);
            noOfBottle = (TextView) itemView.findViewById(R.id.no_of_bottle);
            imgBottle = (ImageView) itemView.findViewById(R.id.img_bottle);

            btnExtValidity = (Button) itemView.findViewById(R.id.btn_extend_validity);
            btnNoOfBar = (Button) itemView.findViewById(R.id.btn_no_of_bar);
            btnGift = (ImageButton) itemView.findViewById(R.id.btn_img_gift);
        }
    }


    private void setColorMethod(float remain, float sum) {

        float marginBar = sum - remain;
        float limitBar = sum / 100;

        if (remain >= (limitBar * 75)) {

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_green));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_green_txt));
            txtQty.setText("" + (int) remain + "ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, marginBar));
            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));

            if (marginBar == 0) {
                txtQtyMargin.setVisibility(View.GONE);
                txtColorMargin.setVisibility(View.GONE);
            } else {
                txtQtyMargin.setVisibility(View.VISIBLE);
                txtColorMargin.setVisibility(View.VISIBLE);
            }
        } else if (remain >= (limitBar * 35)) {
            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_yellow));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_yellow_txt));
            txtQty.setText("" + (int) remain + "ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, remain));
        } else if (remain < (limitBar * 35)) {

            DrawableCompat.setTint(wrappedDrawable, context.getResources().getColor(R.color.liqour_red));
            layoutQtyBack.setBackgroundResource(0);
            layoutQtyBack.setBackgroundResource(R.drawable.back_liquor_qty);
            txtQty.setTextColor(context.getResources().getColor(R.color.liqour_red_txt));
            txtQty.setText("" + (int) remain + "ml left");

            txtQtyMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));

            txtColorMargin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, marginBar));
            txtColor.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, remain));
        }
    }


    private void extendValidityMethod(int position) {

        Universal_Var_Cls.isExtGift = false;

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnExtValidity = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);

        txtHeading.setText("Choose Bottle To Extend Validity");

        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnExtValidity.setText("Extend Validity");
        btnExtValidity.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", listBottle.get(position).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("json_ext_val", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_ext_val", response.toString());

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);


                if (myBarModelCls.getData().size() == 1) {
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, myBarModelCls.getData().get(0).getId());
//                    editor.putFloat(Universal_Var_Cls.priceExtend, myBarModelCls.getData().get(0).getPrice());
                    editor.commit();
                    Universal_Var_Cls.bottlePrice = myBarModelCls.getData().get(0).getPrice();
                    Intent intent = new Intent(context, ExtendValidityActivity.class);
                    context.startActivity(intent);
                } else {

                    listAvailableBottle.addAll(myBarModelCls.getData());

                    for (int i = 0; i < listAvailableBottle.size(); i++) {
                        listSelected.add(false);
                    }
                    adapterExt.notifyDataSetChanged();

                    recyclerView.setHasFixedSize(false);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    recyclerView.setAdapter(adapterExt);

                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_ext_val", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);

        btnExtValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, ExtendValidityActivity.class));
                }
            }
        });
    }

    private void giftBottleMethod(int position) {

        Universal_Var_Cls.isExtGift = false;

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnGiftBottle = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);


        txtHeading.setText("Choose Bottle To Gift");
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnGiftBottle.setText("Gift");
        btnGiftBottle.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", listBottle.get(position).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);


                if (myBarModelCls.getData().size() == 1) {
                    editor.putInt(Universal_Var_Cls.bottlePurchasedId, myBarModelCls.getData().get(0).getId());
                    editor.commit();
                    context.startActivity(new Intent(context, GiftActivity.class));
                } else {

                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);

                    listAvailableBottle.addAll(myBarModelCls.getData());
                    for (int i = 0; i < listAvailableBottle.size(); i++) {
                        listSelected.add(false);
                    }
                    adapterExt.notifyDataSetChanged();

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapterExt);

                    int firstPos = layoutManager.findFirstVisibleItemPosition();
                    int lastPos = layoutManager.findLastVisibleItemPosition();

                    Log.d("visible_items", "first: " + firstPos + "   Last: " + lastPos);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);

        btnGiftBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, GiftActivity.class));
                }
            }
        });
    }


    private void listBottleMethod(int position) {

        Universal_Var_Cls.isExtGift = true;

        /*dialog.show();
        dialog.getWindow().setAttributes(lWindowParams);*/

        TextView txtHeading = (TextView) dialog.findViewById(R.id.heading_dialog);
        ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
        final RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_gift_bottle);
        Button btnGiftBottle = (Button) dialog.findViewById(R.id.gift_bottle_from_dialog);


        txtHeading.setText("Bottle List");
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.bottlePurchasedId);
                editor.commit();
                dialog.dismiss();
            }
        });

        btnGiftBottle.setVisibility(View.GONE);

        JSONObject jsonObject = new JSONObject();
        final List<Datum> listAvailableBottle = new ArrayList<>();
        final List<Boolean> listSelected = new ArrayList<>();
        final MyBarByBottleExtValGiftAdapter adapterExt = new MyBarByBottleExtValGiftAdapter(context, listAvailableBottle, listSelected);

        try {
            jsonObject.put("bottle_id", listBottle.get(position).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.availablePurchasedBottle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);


                listAvailableBottle.addAll(myBarModelCls.getData());
                for (int i = 0; i < listAvailableBottle.size(); i++) {
                    listSelected.add(false);
                }
                adapterExt.notifyDataSetChanged();

                LinearLayoutManager layoutManager = new LinearLayoutManager(context);

                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapterExt);

                int firstPos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastVisibleItemPosition();

                Log.d("visible_items", "first: " + firstPos + "   Last: " + lastPos);

                dialog.show();
                dialog.getWindow().setAttributes(lWindowParams);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);

        /*btnGiftBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getInt(Universal_Var_Cls.bottlePurchasedId, 0) != 0) {
                    dialog.dismiss();
                    context.startActivity(new Intent(context, GiftActivity.class));
                }
            }
        });*/
    }
}
