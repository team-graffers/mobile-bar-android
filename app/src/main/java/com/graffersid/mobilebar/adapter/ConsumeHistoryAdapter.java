package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.model_cls.consume_history.Datum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Sandy on 01/10/2019 at 07:13 PM.
 */
public class ConsumeHistoryAdapter extends RecyclerView.Adapter<ConsumeHistoryAdapter.VwHolder> {

    Context context;
    List<Datum> list;
    LayoutInflater inflater;


    public ConsumeHistoryAdapter(Context context, List<Datum> list) {
        this.context = context;
        this.list = list;

        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_consume_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {

        holder.bottle.setText(list.get(position).getBottleName());
        holder.bar.setText(list.get(position).getBarName());
        try {
            holder.date.setText(new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(list.get(position).getDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.consume.setText(""+list.get(position).getConsumedMl()+"ml");
        holder.chargeable.setText(""+list.get(position).getChargableMl()+"ml");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        TextView bottle, bar, date, consume, chargeable;
        public VwHolder(@NonNull View itemView) {
            super(itemView);


            bottle = (TextView) itemView.findViewById(R.id.consume_bottle);
            bar = (TextView) itemView.findViewById(R.id.consume_bar);
            date = (TextView) itemView.findViewById(R.id.consume_date);
            consume = (TextView) itemView.findViewById(R.id.consume_ml);
            chargeable = (TextView) itemView.findViewById(R.id.consume_charge);
        }
    }
}
