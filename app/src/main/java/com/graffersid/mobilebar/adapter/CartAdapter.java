package com.graffersid.mobilebar.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.CartActivity;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.CartBottle;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sandy on 09/08/2019 at 04:43 PM.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.VwHolder> {

    Context context;
    List<CartBottle> listCart;
    LayoutInflater inflater;
    SQLiteDatabase database;
    CartHelper helper;
    private CartBottle cartBottle;
    CartActivity activity;

    public CartAdapter(Context context, List<CartBottle> listCart) {
        this.context = context;
        this.listCart = listCart;
        inflater = LayoutInflater.from(context);
        activity = (CartActivity) context;

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_cart, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final VwHolder holder, final int position) {

        Picasso.with(context).load(Universal_Var_Cls.baseUrl+listCart.get(position).getImage()).error(R.drawable.bottle_gray).into(holder.imgBottle);

        holder.name.setText(listCart.get(position).getName());
        holder.category.setText(listCart.get(position).getCategory());
        holder.qtyBottle.setText(""+listCart.get(position).getQtyBottle());
        holder.qtyLiqour.setText(""+listCart.get(position).getQtyLiqour()+"ml");
        holder.price.setText("S$ "+listCart.get(position).getPrice());


        holder.incBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qtyBottle.getText().toString()) + 1;
//                Universal_Var_Cls.counter += 1;

                cartBottle = new CartBottle();
                cartBottle.setId(listCart.get(position).getId());
                cartBottle.setQtyBottle(qty);
                cartBottle.setName(listCart.get(position).getName());
                cartBottle.setCategory(listCart.get(position).getCategory());
                cartBottle.setImage(listCart.get(position).getImage());
                cartBottle.setQtyLiqour(listCart.get(position).getQtyLiqour());
                cartBottle.setPrice(listCart.get(position).getPrice());
                listCart.set(position, cartBottle);

                ContentValues values = new ContentValues();
                values.put(CartHelper.KEY_ID, listCart.get(position).getId());
                values.put(CartHelper.KEY_QTY, listCart.get(position).getQtyBottle());
                values.put(CartHelper.KEY_NAME, listCart.get(position).getName());
                values.put(CartHelper.KEY_CATEGORY, listCart.get(position).getCategory());
                values.put(CartHelper.KEY_IMAGE, listCart.get(position).getImage());
                values.put(CartHelper.KEY_PRICE, listCart.get(position).getPrice());
                values.put(CartHelper.KEY_QTY_LIQOUR, listCart.get(position).getQtyLiqour());

                database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + listCart.get(position).getId(), null);

                holder.qtyBottle.setText(""+qty);

                CartActivity.noOfBottles += 1;
                CartActivity.price += listCart.get(position).getPrice();
                CartActivity.qty += listCart.get(position).getQtyLiqour();
                activity.calculateFinalPrice();
            }
        });


        holder.decBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qtyBottle.getText().toString()) - 1;
                CartActivity.noOfBottles -= 1;
 //               Universal_Var_Cls.counter -= 1;

                if (qty > 0) {

                    cartBottle = new CartBottle();
                    cartBottle.setId(listCart.get(position).getId());
                    cartBottle.setQtyBottle(qty);
                    cartBottle.setName(listCart.get(position).getName());
                    cartBottle.setCategory(listCart.get(position).getCategory());
                    cartBottle.setImage(listCart.get(position).getImage());
                    cartBottle.setQtyLiqour(listCart.get(position).getQtyLiqour());
                    cartBottle.setPrice(listCart.get(position).getPrice());
                    listCart.set(position, cartBottle);

                    ContentValues values = new ContentValues();
                    values.put(CartHelper.KEY_ID, listCart.get(position).getId());
                    values.put(CartHelper.KEY_QTY, listCart.get(position).getQtyBottle());
                    values.put(CartHelper.KEY_NAME, listCart.get(position).getName());
                    values.put(CartHelper.KEY_CATEGORY, listCart.get(position).getCategory());
                    values.put(CartHelper.KEY_IMAGE, listCart.get(position).getImage());
                    values.put(CartHelper.KEY_PRICE, listCart.get(position).getPrice());
                    values.put(CartHelper.KEY_QTY_LIQOUR, listCart.get(position).getQtyLiqour());

                    database.update(CartHelper.TABLE_CART, values, CartHelper.KEY_ID + " = " + listCart.get(position).getId(), null);
                    holder.qtyBottle.setText(""+qty);

                    CartActivity.price -= listCart.get(position).getPrice();
                    CartActivity.qty -= listCart.get(position).getQtyLiqour();
                    activity.calculateFinalPrice();
                }else {

                    int id = listCart.get(position).getId();
                    database.delete(CartHelper.TABLE_CART, CartHelper.KEY_ID+"="+id, null);

                    if (CartActivity.noOfBottles == 0){
                        CartActivity.noCartItem.setVisibility(View.VISIBLE);
                    }else {
                        CartActivity.price -= listCart.get(position).getPrice();
                        CartActivity.qty -= listCart.get(position).getQtyLiqour();
                        activity.calculateFinalPrice();
                    }

                    listCart.remove(position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCart.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView imgBottle;
        TextView name, category, qtyLiqour, qtyBottle, price;
        ImageButton incBtn, decBtn;

        public VwHolder(@NonNull View itemView) {
            super(itemView);
            imgBottle = (ImageView) itemView.findViewById(R.id.img_cart_bottle);
            name = (TextView) itemView.findViewById(R.id.bottle_cart_name);
            category = (TextView) itemView.findViewById(R.id.bottle_cart_category);
            qtyLiqour = (TextView) itemView.findViewById(R.id.bottle_cart_liqour_ml);
            decBtn = (ImageButton) itemView.findViewById(R.id.bottle_dec_btn_cart);
            incBtn = (ImageButton) itemView.findViewById(R.id.bottle_inc_btn_cart);
            qtyBottle = (TextView) itemView.findViewById(R.id.bottle_qty_cart);
            price = (TextView) itemView.findViewById(R.id.bottle_price_cart);
        }
    }
}
