package com.graffersid.mobilebar.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.activities.GiftedBottleActivity;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sandy on 07/10/2019 at 03:17 PM.
 */
public class GiftedBottleAdapter extends RecyclerView.Adapter<GiftedBottleAdapter.VwHolder> {

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;
    private final HashMap<String, String> headers;
    Context context;
    List<Result> list;
    LayoutInflater inflater;
    private Dialog dialog;
    private DisplayMetrics displayMetrics;

    public GiftedBottleAdapter(Context context, List<Result> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);

        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();


        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        Universal_Var_Cls.loader(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_gifted_bottle, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, final int position) {

        holder.name.setText(list.get(position).getBottleName());
        holder.category.setText(list.get(position).getCategory());
        holder.mailId.setText(list.get(position).getEmail());

        try {
            String strDate = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yy/MMMM/dd").parse(list.get(position).getDate()));
            holder.dateGift.setText(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (list.get(position).getIsAccepted()){
            holder.imgGift_or_Cancel.setImageResource(R.drawable.ic_gift_done);
        }else {
            holder.imgGift_or_Cancel.setImageResource(R.drawable.ic_gift_waiting);
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).getIsAccepted()){

                    Universal_Var_Cls.mailId = list.get(position).getEmail();
                    GiftedBottleActivity activity = (GiftedBottleActivity) context;
                    activity.detailBottle(list.get(position).getId());
                }else if (!list.get(position).getIsAccepted()){

                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_cancel_gift);
                    ImageButton btnCloseDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
                    Button btnCancelDialog = (Button) dialog.findViewById(R.id.close_dialog2);
                    Button btnCancelGift = (Button) dialog.findViewById(R.id.btn_cancel_gift);

                    displayMetrics = new DisplayMetrics();

                    WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

                    windowmanager.getDefaultDisplay().getMetrics(displayMetrics);


                    int height = Math.round(displayMetrics.heightPixels);

                    height = height - (height * 10 / 100);

                    int width = Math.round(displayMetrics.widthPixels);

                    width = width - (width * 10 / 100);

                    WindowManager.LayoutParams lWindowParams = new WindowManager.LayoutParams();

//        lWindowParams.copyFrom(dialog.getWindow().getAttributes());
                    lWindowParams.width = width;/*WindowManager.LayoutParams.FILL_PARENT;*/ // this is where the magic happens
                    lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;/*height;*/
                    dialog.setCancelable(false);
                    dialog.show();
                    dialog.getWindow().setAttributes(lWindowParams);

                    btnCancelDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    btnCancelGift.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Universal_Var_Cls.dialog.show();
                            String url = Universal_Var_Cls.cancelGift+list.get(position).getId();
                            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    Universal_Var_Cls.dialog.dismiss();

                                    GiftedBottleActivity.list.clear();
                                    GiftedBottleActivity activity = (GiftedBottleActivity) context;
                                    activity.giftedList(Universal_Var_Cls.gifted_list);
                                    dialog.dismiss();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialog.dismiss();
                                    Universal_Var_Cls.dialog.dismiss();
                                    VolleyErrorCls.volleyErr(error, context);
                                }
                            }){
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    return headers;
                                }
                            };
                            RequestQueue queue = Volley.newRequestQueue(context);
                            int timeOut = 15000;
                            DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                            request.setRetryPolicy(retryPolicy);
                            queue.add(request);
                        }
                    });
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        TextView dateGift, name, category, mailId;
        ImageView imgGift_or_Cancel;
        LinearLayout layout;
        public VwHolder(@NonNull View view) {
            super(view);

            dateGift = (TextView) view.findViewById(R.id.date_gift);
            name = (TextView) view.findViewById(R.id.name_bottle);
            category = (TextView) view.findViewById(R.id.name_category);
            mailId = (TextView) view.findViewById(R.id.mail_id_frnd);
            imgGift_or_Cancel = (ImageView) view.findViewById(R.id.gifted_or_cancel);
            layout = (LinearLayout) view.findViewById(R.id.layout_clickable);
        }
    }
}
