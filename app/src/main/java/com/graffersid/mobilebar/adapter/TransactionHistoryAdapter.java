package com.graffersid.mobilebar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.graffersid.mobilebar.model_cls.transaction.HistoryTrans;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandy on 16/09/2019 at 05:05 PM.
 */
public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.VwHolder> {

    Context context;
    List<HistoryTrans> listTransaction;
    LayoutInflater inflater;

    public TransactionHistoryAdapter(Context context, List<HistoryTrans> listTransaction) {
        this.context = context;
        this.listTransaction = listTransaction;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public VwHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VwHolder(inflater.inflate(R.layout.item_transaction_history, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VwHolder holder, int position) {


        int status = listTransaction.get(position).getStatus();

        holder.name_bottle.setText(listTransaction.get(position).getBottleName());

        if (listTransaction.get(position).getImage() != null){

            Picasso.with(context).load(Universal_Var_Cls.baseUrl+listTransaction.get(position).getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(holder.img_bottle);
        }else {
            holder.img_bottle.setImageResource(R.drawable.bottle_gray);
        }
        try {
            holder.date_transaction.setText(new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listTransaction.get(position).getDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (status == 1){

            holder.bottle_transaction_type.setText("Purchased");
            holder.price_points.setText("$"+listTransaction.get(position).getPrice());
            holder.validity_transaction.setVisibility(View.INVISIBLE);
        }else if (status == 2){

            holder.bottle_transaction_type.setText("Renew");
            holder.price_points.setText("$"+listTransaction.get(position).getPrice());
            holder.validity_transaction.setVisibility(View.INVISIBLE);
        }else if (status == 3){

            holder.bottle_transaction_type.setText("Validity Extend");
            holder.price_points.setText("$"+listTransaction.get(position).getPrice());

            try {
                Date startDate = new SimpleDateFormat("dd MM yyyy").parse(new SimpleDateFormat("dd MM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(listTransaction.get(position).getStartDate())));


                Date endDate = new SimpleDateFormat("dd MM yyyy").parse(new SimpleDateFormat("dd MM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(listTransaction.get(position).getEndDate())));

                long difference = endDate.getTime() - startDate.getTime();
                int days = (int) (difference / (1000*60*60*24));
                Log.d("days_days", listTransaction.get(position).getBottleName()+"   "+days);

                int monthExt = 0;
                if (days/365 > 0){
                    monthExt = (days/365)*12;
                    monthExt += (days%365)/30;
                }else {
                    monthExt = days / 30;
                }
                holder.validity_transaction.setText(""+monthExt+" Month");
                holder.validity_transaction.setVisibility(View.VISIBLE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else if (status == 4){

            holder.validity_transaction.setVisibility(View.INVISIBLE);

            holder.bottle_transaction_type.setText(listTransaction.get(position).getName());

            String strPoints = "<font color=#0BAE72>"+listTransaction.get(position).getPoint()+" points</font>";
            holder.price_points.setText(Html.fromHtml(strPoints));
            holder.img_bottle.setImageResource(R.drawable.ic_refer);
        }
    }

    @Override
    public int getItemCount() {
        return listTransaction.size();
    }

    public class VwHolder extends RecyclerView.ViewHolder {

        ImageView img_bottle;
        TextView date_transaction, name_bottle, validity_transaction, bottle_transaction_type, price_points;

        public VwHolder(@NonNull View itemView) {
            super(itemView);

            img_bottle = (ImageView) itemView.findViewById(R.id.img_bottle_tra);
            date_transaction = (TextView) itemView.findViewById(R.id.date_transaction);
            name_bottle = (TextView) itemView.findViewById(R.id.bottle_name_transaction);
            validity_transaction = (TextView) itemView.findViewById(R.id.validity_transaction);
            bottle_transaction_type = (TextView) itemView.findViewById(R.id.bottle_purchased_type_transaction);
            price_points = (TextView) itemView.findViewById(R.id.price_points_transaction);
        }
    }
}
