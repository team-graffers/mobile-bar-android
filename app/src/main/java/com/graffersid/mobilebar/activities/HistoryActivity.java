package com.graffersid.mobilebar.activities;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.frags.BottleConsumeHistoryFragment;
import com.graffersid.mobilebar.frags.BottleHistoryFragment;
import com.graffersid.mobilebar.frags.ConsumeHistoryFragment;
import com.graffersid.mobilebar.frags.TransactionHistoryFragment;

public class HistoryActivity extends AppCompatActivity {

    ImageButton btnBack;
    FragmentTransaction transaction;
    FragmentManager manager;
    BottleHistoryFragment bottleHistoryFragment;
    TransactionHistoryFragment transactionHistoryFragment;
    ConsumeHistoryFragment consumeHistoryFragment;
    BottleConsumeHistoryFragment bottleConsumeHistoryFragment;
    TextView titleTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumption_history);

        btnBack = (ImageButton) findViewById(R.id.back_btn_history);

        titleTxt = (TextView) findViewById(R.id.toolbar_title_history);


        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();

        bottleHistoryFragment = new BottleHistoryFragment();
        transactionHistoryFragment = new TransactionHistoryFragment();
        consumeHistoryFragment = new ConsumeHistoryFragment();
        bottleConsumeHistoryFragment = new BottleConsumeHistoryFragment();


        if (Universal_Var_Cls.statusHistory == 1){

            titleTxt.setText("BOTTLE HISTORY");
            transaction.add(R.id.layout_consume, bottleHistoryFragment);
            transaction.commit();
        }else if (Universal_Var_Cls.statusHistory == 2){

            titleTxt.setText("TRANSACTION HISTORY");
            transaction.add(R.id.layout_consume, transactionHistoryFragment);
            transaction.commit();
        }else if (Universal_Var_Cls.statusHistory == 3){

            titleTxt.setText("CONSUMPTION HISTORY");
            transaction.add(R.id.layout_consume, consumeHistoryFragment);
            transaction.commit();
        }else if (Universal_Var_Cls.isBottle){

            titleTxt.setText("CONSUMPTION HISTORY");
            transaction.add(R.id.layout_consume, bottleConsumeHistoryFragment);
            transaction.commit();
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void startBottleHistory() {

        titleTxt.setText("CONSUMPTION HISTORY");
        transaction = manager.beginTransaction();
        transaction.replace(R.id.layout_consume, bottleConsumeHistoryFragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {

        if (Universal_Var_Cls.isBottle){
            Universal_Var_Cls.isBottle = false;
            super.onBackPressed();
        }else if (bottleConsumeHistoryFragment.isResumed()){

            Universal_Var_Cls.bottleIdPurchased = -1;
            titleTxt.setText("BOTTLE HISTORY");

            transaction = manager.beginTransaction();
            transaction.replace(R.id.layout_consume, bottleHistoryFragment);
            transaction.commit();
        }else {
            super.onBackPressed();
        }
    }
}
