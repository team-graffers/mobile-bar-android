package com.graffersid.mobilebar.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;

public class CheersActivity extends AppCompatActivity {

    ImageButton btnBack;
    TextView remainingLiqour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheers);

        btnBack = (ImageButton) findViewById(R.id.back_btn_cheers);
        remainingLiqour = (TextView) findViewById(R.id.remaining_liqour);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        double x = Universal_Var_Cls.liqourLeft;
        int y = (int) x;

        String leftLiqour = "Thank you for using Mobile Bar. You have <font color=#E82138>"+ y+"ml</font> left in your bottle";

        remainingLiqour.setText(Html.fromHtml(leftLiqour));
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(CheersActivity.this, MyBarActivity.class);

        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
