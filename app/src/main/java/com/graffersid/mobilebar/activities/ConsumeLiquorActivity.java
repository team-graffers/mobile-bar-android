package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.FingerLoginConsume;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.consume.ConversionMl;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ConsumeLiquorActivity extends AppCompatActivity {

    private DisplayMetrics displayMetrics;
    private int height, width;
    private RelativeLayout toolLayout;
    private RelativeLayout toolLayout22;
    private ImageView imgToolLayout;
    private static Context context;
    private LinearLayout contentLayout, contentLayout22;
    ImageButton btnBack;
    TextView titleBottleName, bottleName, categoryName, remainingMl, barName, barAddress;
    ImageView imgBottle, imgBar;
    HashMap<String, String> headers;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    EditText consumedMlInBar, customerPin, barDiscount, barPin;
    LinearLayout layoutCalculateLiqour;
    TextView txtCalculatedLiqour, bottleInBar, bottleInBar22, mlConsumed, mlChargeable, mlTotalChargeable;
    ImageButton btnFingerCustomerPin;
    private Button btnConsumeLiqour, btnCheckoutBar;
    private static int ml;
    private String tokenDrink;



    double conversionRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_liquor);
    }


    @Override
    protected void onStart() {
        super.onStart();

        initialize();

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        conversionRate = 0.0;

        ml = 0;


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        barBottleDetail();



        consumedMlInBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().length()>0){
                    ml = Integer.parseInt(charSequence.toString());

                    if (Universal_Var_Cls.liqourLeft >= ml) {
                        layoutCalculateLiqour.setVisibility(View.VISIBLE);

                        int  sxsx = (int) Math.ceil(ml * conversionRate);
                        txtCalculatedLiqour.setText("" + ml + "ml at Bar = " + sxsx + "ml of Bottle");
                    }else {
                        consumedMlInBar.setError("You have insufficient liquor in your bottle");
                    }
                }else {
                    ml = 0;
                    layoutCalculateLiqour.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btnConsumeLiqour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*int ml = Integer.parseInt(consumedMlInBar.getText().toString());*/

                if (ml > 0 && Universal_Var_Cls.liqourLeft >= Math.ceil(ml*conversionRate)) {

     //               if (Universal_Var_Cls.pinCustomer == Integer.parseInt(customerPin.getText().toString())){

                        verifyCustomerPin();

                        /*contentLayout.setVisibility(View.GONE);
                        contentLayout22.setVisibility(View.VISIBLE);

                        mlConsumed.setText(consumedMlInBar.getText().toString()+"ml");
                        int sxsx = (int) Math.ceil(ml * conversionRate);
                        mlChargeable.setText(""+sxsx+"ml");
                        mlTotalChargeable.setText(""+sxsx+"ml");*/
                /*    }else {
                        customerPin.setError("Enter correct PIN");
                    }*/
                }else {
                    if (ml>0) {
                        consumedMlInBar.setError("You have insufficient liquor in your bottle");
                    }
                    else {
                 //       consumedMlInBar.setError("Enter liquor quantity");
                    }
                }
            }
        });

        barDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().length()>0){

                    double dis = Double.parseDouble(charSequence.toString());
                    String sttt = mlChargeable.getText().toString().substring(0, mlChargeable.getText().toString().length()-2);
                    double ml = Double.parseDouble(sttt);

                    int sxsx = (int) Math.ceil(ml - (ml*dis/100));
                    mlTotalChargeable.setText(""+sxsx+"ml");
                }else {
                    int ml = Integer.parseInt(consumedMlInBar.getText().toString());

                    int sxsx = (int) Math.ceil(ml * conversionRate);
                    mlTotalChargeable.setText(""+sxsx+"ml");
 //                   layoutCalculateLiqour.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnCheckoutBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (barPin.getText().toString().trim().length() > 0) {


                    /*int ml = Integer.parseInt(consumedMlInBar.getText().toString());
                    int dis = 0;
                    if (barDiscount.getText().toString().length() > 0){
                        dis = Integer.parseInt(barDiscount.getText().toString());
                    }

                    String sxsx = String.valueOf(ml);
                    final int sxsx22 = (int) Math.ceil((ml * conversionRate) - (ml*conversionRate*dis/100));

                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.put("bar_id", Universal_Var_Cls.barId);
                        jsonObject.put("customerbottle_id", Universal_Var_Cls.customerBottleId);
                        jsonObject.put("bar_pin", barPin.getText().toString().trim());
                        jsonObject.put("chargable_ml", sxsx22);
                        jsonObject.put("consumed_ml", sxsx);
                        jsonObject.put("bar_discount", dis);
                        jsonObject.put("token", tokenDrink);
                        jsonObject.put("created_date_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(ConsumeLiquorActivity.this, PaymentActivity.class);
                    intent.putExtra("jsonCheckOut", jsonObject.toString());
                    startActivity(intent);*/

                    verifyCustomerPin();
     //               finalCheckout();
                }
            }
        });


        btnFingerCustomerPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

 //               new FingerLoginConsume(ConsumeLiquorActivity.this).fingerPrintLogin();
            }
        });
 //       new FingerLoginConsume(ConsumeLiquorActivity.this).fingerPrintLogin();
    }

    private void initialize() {

        context = ConsumeLiquorActivity.this;

        Universal_Var_Cls.loader(context);

        displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        height = Math.round(displayMetrics.heightPixels/* / displayMetrics.density*/);
        width = Math.round(displayMetrics.widthPixels/* / displayMetrics.density*/);

        toolLayout = (RelativeLayout) findViewById(R.id.tool_layout);
        toolLayout22 = (RelativeLayout) findViewById(R.id.tool_layout22);
        contentLayout = (LinearLayout) findViewById(R.id.layout_content);
        contentLayout22 = (LinearLayout) findViewById(R.id.layout_content22);
        imgToolLayout = (ImageView) findViewById(R.id.img_tool_layout);

        imgBottle = (ImageView) findViewById(R.id.img_bottle_consume);
        imgBar = (ImageView) findViewById(R.id.img_bar_consume);

        titleBottleName = (TextView) findViewById(R.id.name_bottle_consume);

        bottleName = (TextView) findViewById(R.id.bottle_name);
        categoryName = (TextView) findViewById(R.id.category_name);
        remainingMl = (TextView) findViewById(R.id.remaining);

        barName = (TextView) findViewById(R.id.bar_name);
        barAddress = (TextView) findViewById(R.id.bar_address);

        bottleInBar = (TextView) findViewById(R.id.bottle_at_bar);
        bottleInBar22 = (TextView) findViewById(R.id.bottle_at_bar22);

        btnBack = (ImageButton) findViewById(R.id.btn_back_consume);

        consumedMlInBar = (EditText) findViewById(R.id.consumed_ml_in_bar);
        layoutCalculateLiqour = (LinearLayout) findViewById(R.id.calculate_liqour);
        txtCalculatedLiqour = (TextView) findViewById(R.id.calculate_liqour_txt);

        customerPin = (EditText) findViewById(R.id.customer_pin);
        btnFingerCustomerPin = (ImageButton) findViewById(R.id.finger_img_btn_customer_pin);
        btnConsumeLiqour = (Button) findViewById(R.id.btn_checkout_after_consume);

        barDiscount = (EditText) findViewById(R.id.bar_discount);
        barPin = (EditText) findViewById(R.id.pin_bar);

        mlConsumed = (TextView) findViewById(R.id.ml_consumed);
        mlChargeable = (TextView) findViewById(R.id.ml_chargeable);
        mlTotalChargeable = (TextView) findViewById(R.id.ml_total_chargeable);

        btnCheckoutBar = (Button) findViewById(R.id.btn_checkout_from_bar);


        int stHeight = Universal_Var_Cls.getStatusBarHeight(context) + 10;

        toolLayout.setPadding(0, stHeight,0, 0);

        ViewGroup.LayoutParams params = toolLayout22.getLayoutParams();
        params.height = height*40/100;
        toolLayout22.setLayoutParams(params);


        RelativeLayout.LayoutParams params22 = (RelativeLayout.LayoutParams) contentLayout.getLayoutParams();
        params22.setMargins(0, params.height-120, 0, 0);

        contentLayout.setLayoutParams(params22);

        params22 = (RelativeLayout.LayoutParams) contentLayout22.getLayoutParams();
        params22.setMargins(0, params.height-120, 0, 0);
        contentLayout22.setLayoutParams(params22);

        contentLayout22.setVisibility(View.GONE);
    }

    public void verifyCustomerPin() {

        tokenDrink = "";

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("pin", Universal_Var_Cls.pinCustomer);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("resp_verify_cust_pin", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.generateDrinkToken, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_verify_cust_pin", response.toString());
                Universal_Var_Cls.dialog.dismiss();

                try {
                    if (response.getBoolean("status")){

                        contentLayout.setVisibility(View.GONE);
                        contentLayout22.setVisibility(View.VISIBLE);

                        mlConsumed.setText(consumedMlInBar.getText().toString()+"ml");
                        int sxsx = (int) Math.ceil(ml * conversionRate);
                        mlChargeable.setText(""+sxsx+"ml");
                        mlTotalChargeable.setText(""+sxsx+"ml");

                        String tokii = response.getString("token");
                        String[] tok = tokii.split(":");

                        String tokenD = "";
                        for (int i =1; i<tok.length; i++){
                            if (i == tok.length -1){
                                tokenD = tokenD + tok[i];
                            }else {
                                tokenD = tokenD + tok[i]+":";
                            }
                        }
                        tokenDrink = tokenD;
                        finalCheckout();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                Log.d("resp_verify_cust_pin_e", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(ConsumeLiquorActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }


    @Override
    public void onBackPressed() {
        Universal_Var_Cls.pinCustomer = 0;

        Intent intent = new Intent(ConsumeLiquorActivity.this, MyBarActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void barBottleDetail() {

        Universal_Var_Cls.dialog.show();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bar_id", Universal_Var_Cls.barId);
            jsonObject.put("bottle_id", Universal_Var_Cls.bottleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json_consume", jsonObject.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.consumeBarBottleDetail, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                Log.d("resp_consume_s", response.toString());
                ConversionMl conversionMl = new Gson().fromJson(response.toString(), ConversionMl.class);

                setBarBottle(conversionMl);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void setBarBottle(ConversionMl conversionMl) {

        if (conversionMl.getBottle().getImage() != null){

            String imgUrl = Universal_Var_Cls.baseUrl+conversionMl.getBottle().getImage();
            Log.d("img_url", imgUrl);

            Picasso.with(context).load(imgUrl).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgBottle);
        }
        if (conversionMl.getBar().getImage() != null){
            Picasso.with(context).load(Universal_Var_Cls.baseUrl+conversionMl.getBar().getImage()).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgBar);
        }
        if (conversionMl.getBottle().getName() != null){
            titleBottleName.setText(conversionMl.getBottle().getName());
            bottleName.setText(conversionMl.getBottle().getName());
        }
        if (conversionMl.getBottle().getCategory() != null){
            categoryName.setText(conversionMl.getBottle().getCategory());
        }
        if (conversionMl.getBottle().getRemainingMl() != null && conversionMl.getBottle().getTotalMl() != null){
            int percent = (int) (conversionMl.getBottle().getRemainingMl() * 100 / conversionMl.getBottle().getTotalMl());
            remainingMl.setText(""+percent+"% left");
        }
        if (conversionMl.getBar().getName() != null){
            barName.setText(conversionMl.getBar().getName());
        }
        if (conversionMl.getBar().getAddress() != null){
            barAddress.setText(conversionMl.getBar().getAddress());
        }


        if (conversionMl.getBottle().getName() != null && conversionMl.getBar().getName() != null){
            bottleInBar.setText(conversionMl.getBottle().getName()+" at "+conversionMl.getBar().getName());
        }

        conversionRate = conversionMl.getBarbottle().getConversionMl();

        Universal_Var_Cls.liqourLeft = conversionMl.getBottle().getRemainingMl();
    }

    private void finalCheckout() {

        Universal_Var_Cls.dialog.show();

        int ml = Integer.parseInt(consumedMlInBar.getText().toString());
        int dis = 0;
        if (barDiscount.getText().toString().length() > 0){
            dis = Integer.parseInt(barDiscount.getText().toString());
        }

        String sxsx = String.valueOf(ml);
        final int sxsx22 = (int) Math.ceil((ml * conversionRate) - (ml*conversionRate*dis/100));

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bar_id", Universal_Var_Cls.barId);
            jsonObject.put("customerbottle_id", Universal_Var_Cls.customerBottleId);
            jsonObject.put("bar_pin", barPin.getText().toString().trim());
            jsonObject.put("chargable_ml", sxsx22);
            jsonObject.put("consumed_ml", sxsx);
            jsonObject.put("bar_discount", dis);
            jsonObject.put("token", tokenDrink);
            jsonObject.put("created_date_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("consumed_json", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.orderDrinkCreateView, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();
                Universal_Var_Cls.pinCustomer = 0;
                Log.d("consumed_liqour", response.toString());

                Universal_Var_Cls.liqourLeft -= sxsx22;

                startActivity(new Intent(ConsumeLiquorActivity.this, CheersActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                Log.d("consumed_liqour_er", error.toString());

                /*if (error instanceof NetworkError){
                    Log.d("consumed_liqour_er", error.networkResponse.data.toString());
                }else if (error instanceof ServerError){
                    Log.d("consumed_liqour_er", error.getLocalizedMessage());
                }*/
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}