package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.MyBarByBarAdapter;
import com.graffersid.mobilebar.adapter.MyBarByBottleAdapter;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyBarActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    private SearchView searchView;
    RecyclerView recyclerVwByBottle, recyclerVwByBar;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private EditText searchEditText;
    ImageButton btnBottleList;
    TextView noOfBottleBrand, noOfBars;
    private SwipeRefreshLayout swipeRefreshLayout;

    private CircleImageView imgUser;
    private TextView nameUser, someThingWentWrong, someThingWrong;
    private LinearLayout noBarBottleAvailable, listLayout;
    private Button btnEditProfile, btnMyBar, btnBottleHistory, btnTransactionHistory, btnConsumptionHistory, btnDeals, btnGifts, btnReferFriend, btnT_and_C, btnSignOut, btnByBottle, btnByBar;
    private Context context;

    MyBarByBarAdapter adapterByBar;
    MyBarByBottleAdapter adapterByBottle;
    List<Result> listBar;
    List<Result> listBottle;
    HashMap<String, String> headers;

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    String nextPage, previousPage;

    private boolean isScrollingByBottle = false, isPaginateByBottle = false;
    private int currentItemsByBottle, totalItemsByBottle, scrollOutItemsByBottle;
    private GridLayoutManager layoutManagerBottle;
    private static String nextPageByBottle;
    private static int statusLay;
    private CartHelper helper;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bar);

        context = MyBarActivity.this;
        new CurrentLocation(context).lattLong();

        statusLay = 0;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //     toggle.getDrawerArrowDrawable().setColor(Color.RED);
        toolbar.setNavigationIcon(R.drawable.menu_white);
        //    toggle.setHomeAsUpIndicator(R.drawable.menu_black);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Universal_Var_Cls.loader(context);

        nextPage = null;

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();


        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        recyclerVwByBottle = (RecyclerView) findViewById(R.id.recycler_view_by_bottle);
        recyclerVwByBar = (RecyclerView) findViewById(R.id.recycler_view_by_bar);
        btnBottleList = (ImageButton) findViewById(R.id.btn_bottle_list);

        noOfBottleBrand = (TextView) findViewById(R.id.no_of_bottle_brand);
        noOfBars = (TextView) findViewById(R.id.no_of_bars);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);


        recyclerVwByBar.setPadding(0, 0, 0, 160);
        recyclerVwByBottle.setPadding(0, 0, 0, 160);


        imgUser = (CircleImageView) findViewById(R.id.img_user);
        nameUser = (TextView) findViewById(R.id.name_user);
        someThingWentWrong = (TextView) findViewById(R.id.something_went_wrong);
        someThingWrong = (TextView) findViewById(R.id.something_wrong);

        noBarBottleAvailable = (LinearLayout) findViewById(R.id.no_bar_bottle_avail);
        listLayout = (LinearLayout) findViewById(R.id.list_layout);

        btnEditProfile = (Button) findViewById(R.id.edit_profile_btn);
        btnMyBar = (Button) findViewById(R.id.my_bar_btn);
        btnBottleHistory = (Button) findViewById(R.id.bottle_history_btn);
        btnTransactionHistory = (Button) findViewById(R.id.transaction_history_btn);
        btnConsumptionHistory = (Button) findViewById(R.id.consumption_history_btn);
        btnDeals = (Button) findViewById(R.id.deals_btn);
        btnGifts = (Button) findViewById(R.id.gifts_btn);
        btnReferFriend = (Button) findViewById(R.id.refer_friend_btn);
        btnT_and_C = (Button) findViewById(R.id.t_and_c_btn);
        btnSignOut = (Button) findViewById(R.id.sign_in_out_btn);

        btnByBottle = (Button) findViewById(R.id.btn_by_bottle);
        btnByBar = (Button) findViewById(R.id.btn_by_bar);


 /*       recyclerVwByBottle.setPadding(0, 0, 0, 40);
        recyclerVwByBar.setPadding(0, 0, 0, 40);*/


        if (preferences.getString(Universal_Var_Cls.imgUrl, null) != null && preferences.getString(Universal_Var_Cls.imgUrl, null).length() > 0) {
            Picasso.with(this).load(preferences.getString(Universal_Var_Cls.imgUrl, null)).placeholder(R.drawable.bottle_gray).error(R.drawable.bottle_gray).into(imgUser);
        }

        String userProfile = preferences.getString(Universal_Var_Cls.loginStr, null);
        try {
            JSONObject jsonObject = new JSONObject(userProfile);
            nameUser.setText(jsonObject.getJSONObject("user").getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("token_token", preferences.getString(Universal_Var_Cls.toKen, null));

        listBar = new ArrayList<>();
        adapterByBar = new MyBarByBarAdapter(context, listBar);

        layoutManager = new GridLayoutManager(context, 1);
        recyclerVwByBar.setHasFixedSize(true);
        recyclerVwByBar.setLayoutManager(layoutManager);
        recyclerVwByBar.setAdapter(adapterByBar);

        listBottle = new ArrayList<>();
        adapterByBottle = new MyBarByBottleAdapter(context, listBottle);

        layoutManagerBottle = new GridLayoutManager(context, 1);
        recyclerVwByBottle.setHasFixedSize(true);
        recyclerVwByBottle.setLayoutManager(layoutManagerBottle);
        recyclerVwByBottle.setAdapter(adapterByBottle);

        recyclerVwByBar.setVisibility(View.GONE);
        recyclerVwByBottle.setVisibility(View.VISIBLE);

        if (statusLay == 1){
            recyclerVwByBar.setVisibility(View.GONE);
            recyclerVwByBottle.setVisibility(View.VISIBLE);
        }else if (statusLay == 2){
            recyclerVwByBar.setVisibility(View.VISIBLE);
            recyclerVwByBottle.setVisibility(View.GONE);
        }

        someThingWrong.setVisibility(View.GONE);

        recyclerViewEvents();

        clickMethod();

        byBottleList(Universal_Var_Cls.myBarByBottle);

        byBarList(Universal_Var_Cls.myBarByBar);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        System.exit(0);
        finish();
    }

    private void recyclerViewEvents() {

        recyclerVwByBar.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                byBarList(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });

        recyclerVwByBottle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrollingByBottle = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItemsByBottle = layoutManagerBottle.getChildCount();
                totalItemsByBottle = layoutManagerBottle.getItemCount();
                scrollOutItemsByBottle = layoutManagerBottle.findFirstVisibleItemPosition();

                if (isScrollingByBottle && (currentItemsByBottle + scrollOutItemsByBottle == totalItemsByBottle)) {

                    isScrollingByBottle = false;
                    if (nextPageByBottle != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginateByBottle = true;
                                byBottleList(nextPageByBottle);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void clickMethod() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onStart();
            }
        });

        btnBottleList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyBarActivity.this, LiquorActivity.class));
            }
        });

        btnByBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CurrentLocation(context).lattLong();

                noOfBottleBrand.setVisibility(View.VISIBLE);
                noOfBars.setVisibility(View.GONE);

                statusLay = 1;

                searchEditText.setHint("Search Liqour");

                searchEditText.setText("");

                recyclerVwByBar.setVisibility(View.GONE);
                recyclerVwByBottle.setVisibility(View.VISIBLE);
                someThingWentWrong.setVisibility(View.GONE);

                if (listBottle.size()>0){
                    noBarBottleAvailable.setVisibility(View.GONE);
                    listLayout.setVisibility(View.VISIBLE);
                }else {
                    noBarBottleAvailable.setVisibility(View.VISIBLE);
                    listLayout.setVisibility(View.GONE);
                }
                btnByBottle.setBackgroundResource(R.drawable.ic_toggle_btn);
                btnByBar.setBackgroundResource(android.R.color.transparent);
                btnByBottle.setTextColor(getResources().getColor(R.color.white));
                btnByBar.setTextColor(getResources().getColor(R.color.heading_liqour));
                if (nextPageByBottle != null){
                    byBottleList(nextPageByBottle);
                }else if (listBottle.isEmpty()){
                    byBottleList(Universal_Var_Cls.myBarByBottle);
                }
            }
        });
        btnByBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CurrentLocation(context).lattLong();

                noOfBottleBrand.setVisibility(View.GONE);
                noOfBars.setVisibility(View.VISIBLE);

                statusLay = 2;

                searchEditText.setHint("Search Bar");

                searchEditText.setText("");

                recyclerVwByBar.setVisibility(View.VISIBLE);
                recyclerVwByBottle.setVisibility(View.GONE);

                btnByBottle.setBackgroundResource(android.R.color.transparent);
                btnByBar.setBackgroundResource(R.drawable.ic_toggle_btn);
                btnByBottle.setTextColor(getResources().getColor(R.color.heading_liqour));
                btnByBar.setTextColor(getResources().getColor(R.color.white));

                if (nextPage != null){
                    byBarList(nextPage);
                }else if (listBar.isEmpty()) {
                    byBarList(Universal_Var_Cls.myBarByBar);
                }
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.remove(Universal_Var_Cls.toKen);
                editor.remove(Universal_Var_Cls.imgUrl);
                editor.commit();
                imgUser.setBackgroundResource(R.drawable.default_img);

                database.delete(CartHelper.TABLE_CART, null, null);
                startActivity(new Intent(MyBarActivity.this, LiquorActivity.class));
            }
        });
        btnDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                startActivity(new Intent(MyBarActivity.this, DealsActivity.class));
            }
        });
        btnMyBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
            }
        });
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                startActivity(new Intent(MyBarActivity.this, ProfileActivity.class));
            }
        });
        btnBottleHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                Universal_Var_Cls.statusHistory = 1;
                startActivity(new Intent(MyBarActivity.this, HistoryActivity.class));
            }
        });
        btnTransactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                Universal_Var_Cls.statusHistory = 2;
                startActivity(new Intent(MyBarActivity.this, HistoryActivity.class));
            }
        });

        btnConsumptionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                Universal_Var_Cls.statusHistory = 3;
                startActivity(new Intent(MyBarActivity.this, HistoryActivity.class));
            }
        });
        btnGifts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                startActivity(new Intent(MyBarActivity.this, GiftedBottleActivity.class));
            }
        });
        btnReferFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                startActivity(new Intent(MyBarActivity.this, ReferFriendActivity.class));
            }
        });
    }

    private void closeDrawer(){
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void byBottleList(final String url) {

        if (isPaginateByBottle){

        }else {
    //        Universal_Var_Cls.dialog.show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                Log.d("resp_by_bottle_s", response.toString());

                someThingWrong.setVisibility(View.GONE);

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getResults() != null && !myBarModelCls.getResults().isEmpty()){

                    noOfBottleBrand.setText("Total Brands: "+myBarModelCls.getCount());

                    listLayout.setVisibility(View.VISIBLE);
                    noBarBottleAvailable.setVisibility(View.GONE);

                    Result result;

                    listBottle.addAll(myBarModelCls.getResults());
                    adapterByBottle.notifyDataSetChanged();

                    if (myBarModelCls.getNext() != null){

                        String str = myBarModelCls.getNext();

                        if (str.contains("localhost")){
                            str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                        }
                        nextPageByBottle = str;
                    }else {
                        nextPageByBottle = null;
                    }
                    /*if (myBarModelCls.getNext() != null){
                        nextPageByBottle = myBarModelCls.getNext();
                    }else {
                        nextPageByBottle = null;
                    }*/
                }else {
                    listLayout.setVisibility(View.GONE);
                    noBarBottleAvailable.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
                Log.d("resp_by_bottle_err", error.toString());
                Log.d("resp_by_bottle_err", url);
                VolleyErrorCls.volleyErr(error, context);

                if (listBottle.size() > 0){
                }else {
                    listLayout.setVisibility(View.GONE);
                    someThingWrong.setVisibility(View.VISIBLE);
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void byBarList(final String url) {

        if (isPaginate){

        }else {
   //         Universal_Var_Cls.dialog.show();
        }

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("xcord", Universal_Var_Cls.lat);
            jsonObject.put("ycord", Universal_Var_Cls.lng);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("mybarbybar_json", ""+url+"\n"+jsonObject.toString());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                Log.d("resp_by_bar", response.toString());

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getResults() != null && !myBarModelCls.getResults().isEmpty()){

//                    recyclerVwByBar.setVisibility(View.VISIBLE);
                    someThingWentWrong.setVisibility(View.GONE);

                    Result result;

                    noOfBars.setText("Total Bars: "+myBarModelCls.getCount());

                    listBar.addAll(myBarModelCls.getResults());
                    adapterByBar.notifyDataSetChanged();
                    if (myBarModelCls.getNext() != null){

                        String str = myBarModelCls.getNext();

                        if (str.contains("localhost")){
                            str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                        }
                        nextPage = str;
                    }else {
                        nextPage = null;
                    }
                }else {
                    recyclerVwByBar.setVisibility(View.GONE);
   //                 recyclerVwByBottle.setVisibility(View.GONE);
                    someThingWentWrong.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (Universal_Var_Cls.dialog.isShowing()) {
                    Universal_Var_Cls.dialog.dismiss();
                }
                if (swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }

                Log.d("resp_by_bar_err", error.toString());

                VolleyErrorCls.volleyErr(error, context);

                if (listBar.size() > 0){
                }else {
                    recyclerVwByBar.setVisibility(View.GONE);
                    someThingWentWrong.setVisibility(View.VISIBLE);
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my_bar, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        searchEditText.setTextColor(Color.parseColor("#000000"));
        searchEditText.setHintTextColor(Color.parseColor("#777777"));
        searchEditText.setBackgroundResource(R.drawable.back_category_filter);


        try {

            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            field.set(searchEditText, R.drawable.cursor_width_height);

            /*// Get the editor
            field = TextView.class.getDeclaredField(String.valueOf(android.support.v7.appcompat.R.id.search_src_text));
            field.setAccessible(true);
            Object editor = field.get(searchEditText);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(searchEditText.getContext(), drawableResId);
            drawable.setColorFilter(getResources().getColor(R.color.heading_liqour), PorterDuff.Mode.SRC_IN);
            Drawable[] drawables = {drawable, drawable};

            // Set the drawables
            field = editor.getClass().getDeclaredField("mCursorDrawable");
            field.setAccessible(true);
            field.set(editor, drawables);*/
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if(recyclerVwByBottle.getVisibility() == View.VISIBLE) {

            searchEditText.setHint("Search Liqour");
        }else if(recyclerVwByBar.getVisibility() == View.VISIBLE){
            searchEditText.setHint("Search Bar");
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                if(recyclerVwByBottle.getVisibility() == View.VISIBLE) {

                    listBottle.clear();
                    byBottleList(Universal_Var_Cls.myBarByBottle+"?name="+s);
                }else if(recyclerVwByBar.getVisibility() == View.VISIBLE){

                    listBar.clear();
                    byBarList(Universal_Var_Cls.myBarByBar+"?name="+s);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (s.length() == 0){
                    if(recyclerVwByBottle.getVisibility() == View.VISIBLE) {

                        listBottle.clear();
                        byBottleList(Universal_Var_Cls.myBarByBottle+"?name="+s);
                    }else if(recyclerVwByBar.getVisibility() == View.VISIBLE){

                        listBar.clear();
                        byBarList(Universal_Var_Cls.myBarByBar+"?name="+s);
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}
