package com.graffersid.mobilebar.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.FingerLoginConsume;
import com.graffersid.mobilebar.classes.FingerprintHandler;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.login_model_cls.fb_login.FbLogin_Model_Cls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class SignInUpActivity extends AppCompatActivity {

    private static final String KEY_NAME = "mobileBar";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    Button signUpBtn, fbLoginBtn, googleLoginBtn, fbSignUpBtn, googleSignUpBtn, signInBtn;
    CheckBox ageVerify;
    EditText referralCode;
    LinearLayout signInLayout, signUpLayout;
    private View mContentView;
    ImageButton fingerImgBtn;
    LoginButton loginButton;
    private CallbackManager callbackManager;
    Context context;

    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    boolean boolSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_up);

        mContentView = findViewById(R.id.fullscreen_sign_in);
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        context = SignInUpActivity.this;

        signUpBtn = (Button) findViewById(R.id.sign_up_btn);

        fingerImgBtn = (ImageButton) findViewById(R.id.finger_img_btn);
        fbLoginBtn = (Button) findViewById(R.id.fb_sign_in);
        googleLoginBtn = (Button) findViewById(R.id.google_sign_in);
        fbSignUpBtn = (Button) findViewById(R.id.fb_sign_up_btn);
        googleSignUpBtn = (Button) findViewById(R.id.google_sign_up_btn);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        signInBtn = (Button) findViewById(R.id.sign_in_btn);
        ageVerify = (CheckBox) findViewById(R.id.age_verify_btn);
        signInLayout = (LinearLayout) findViewById(R.id.sign_in_layout);
        signUpLayout = (LinearLayout) findViewById(R.id.sign_up_layout);
        referralCode = (EditText) findViewById(R.id.referral_code_edt_txt);


        if (ageVerify.isChecked()) {
            fbSignUpBtn.setBackgroundResource(R.drawable.back_fb_sign_in_btn);
            googleSignUpBtn.setBackgroundResource(R.drawable.back_google_sign_in_btn);
            fbSignUpBtn.setEnabled(true);
            googleSignUpBtn.setEnabled(true);
        } else {
            fbSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
            googleSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
            fbSignUpBtn.setEnabled(false);
            googleSignUpBtn.setEnabled(false);
        }

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        boolSignUp = false;


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInLayout.setVisibility(View.GONE);
                signUpLayout.setVisibility(View.VISIBLE);
                Universal_Var_Cls.isSignUp = true;
            }
        });
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpLayout.setVisibility(View.GONE);
                signInLayout.setVisibility(View.VISIBLE);
                Universal_Var_Cls.isSignUp = false;
            }
        });
        ageVerify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    fbSignUpBtn.setBackgroundResource(R.drawable.back_fb_sign_in_btn);
                    googleSignUpBtn.setBackgroundResource(R.drawable.back_google_sign_in_btn);
                    fbSignUpBtn.setEnabled(true);
                    googleSignUpBtn.setEnabled(true);
                } else {
                    fbSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
                    googleSignUpBtn.setBackgroundResource(R.drawable.back_google_fb_btn);
                    fbSignUpBtn.setEnabled(false);
                    googleSignUpBtn.setEnabled(false);
                }
            }
        });

        fingerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();
 //               fingerPrintLogin();
            }
        });


        fbLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolSignUp = false;
                loginButton.performClick();
            }
        });
        fbSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolSignUp = true;
                loginButton.performClick();
            }
        });
        fbLoginMethod();
        googleLoginMethod();

        Universal_Var_Cls.isSignIn = true;

        if (preferences.getString(Universal_Var_Cls.loginStr, null) != null){
            new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();
        }
    }

    private void googleLoginMethod() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolSignUp = false;
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        googleSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolSignUp = true;
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }

    /*@Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }*/

    private void fbLoginMethod() {

        callbackManager = CallbackManager.Factory.create();

        AccessTokenTracker tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

                if (currentAccessToken == null) {

                } else {
                    String userId = currentAccessToken.getUserId();
                    String token = currentAccessToken.getToken();
                    //                   Log.d("jkjdhkada", "Hi\n"+userId);
                }
            }
        };


        loginButton.setReadPermissions(Arrays.asList("email"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loadUserProfile(loginResult);
            }

            @Override
            public void onCancel() {
                //         Toast.makeText(SignInUpActivity.this, "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(SignInUpActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                Log.d("fb_error", ""+error);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                String personName = account.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();

                String personId = acct.getId();

                Uri personPhoto = null;
                if (acct.getPhotoUrl() != null) {
                    personPhoto = acct.getPhotoUrl();
                }

                Log.d("google_resp", "Hi" + "\npersonName : " + personName + "\npersonGivenName : " + personGivenName + "\npersonFamilyName : " + personFamilyName + "\npersonEmail : " + personEmail + "\npersonId : " + personId + "\npersonPhoto : " + personPhoto);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("google", personId);
                    jsonObject.put("email", personEmail);
                    JSONObject jsonObject22 = new JSONObject();
                    jsonObject22.put("name", personName);
                    if (personPhoto != null) {
                        jsonObject22.put("image", personPhoto);
                    } else {
                        jsonObject22.put("image", "");
                    }
                    jsonObject22.put("phone", JSONObject.NULL);

                    new CurrentLocation(SignInUpActivity.this).lattLong();
                    jsonObject22.put("xcord", Universal_Var_Cls.lat);
                    jsonObject22.put("ycord", Universal_Var_Cls.lng);

                    if (referralCode.getText() != null) {
                        jsonObject22.put("reffer_code", referralCode.getText().toString());
                    } else {
                        jsonObject22.put("reffer_code", "");
                    }
                    jsonObject.put("user", jsonObject22);

                    Log.d("google_resp", jsonObject.toString());

//                    LoginManager.getInstance().logOut();

                    mGoogleSignInClient.signOut()
                            .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    // ...
                                }
                            });


                    if (personName != null) {
                        signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithGoogle, "google");
                    }else {
           //             googleLoginBtn.callOnClick();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            /*mGoogleSignInClient.signOut()
                    .addOnCompleteListener(SignInUpActivity.this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                        }
                    });*/
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.


            String sssts = e.getMessage()+"   "+e.getStatusMessage();
            Log.w("google_resp_e", "" + e);
            //          updateUI(null);
            Toast.makeText(context, "Some thing went wrong. Please try again", Toast.LENGTH_SHORT).show();
        }
    }


    private void loadUserProfile(LoginResult newAccessToken) {

        GraphRequest request = GraphRequest.newMeRequest(newAccessToken.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                FbLogin_Model_Cls fbLogin_model_cls = new Gson().fromJson(object.toString(), FbLogin_Model_Cls.class);

                JSONObject jsonObject = new JSONObject();
                try {
                    String firstName = object.getString("name");
                    String email = object.getString("email");
                    String id = object.getString("id");
                    //             String contact_number = object. getString("contact_number");
                    String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=normal";

                    Log.d("jkjdhkada", "Hi\n" + "Name: " + firstName + "\nemail: " + email + "\nid: " + id + "\nimageUrl: " + imageUrl);


                    jsonObject.put("facebook", fbLogin_model_cls.getId());
                    jsonObject.put("email", fbLogin_model_cls.getEmail());
                    JSONObject jsonObject22 = new JSONObject();
                    jsonObject22.put("name", fbLogin_model_cls.getName());

                    if (fbLogin_model_cls.getPicture() != null) {
                        jsonObject22.put("image", fbLogin_model_cls.getPicture());
                    } else {
                        jsonObject22.put("image", "");
                    }

                    new CurrentLocation(SignInUpActivity.this).lattLong();
                    jsonObject22.put("xcord", Universal_Var_Cls.lat);
                    jsonObject22.put("ycord", Universal_Var_Cls.lng);
                    if (referralCode.getText() != null) {
                        jsonObject22.put("reffer_code", referralCode.getText().toString());
                    } else {
                        jsonObject22.put("reffer_code", "");
                    }
                    jsonObject22.put("phone", JSONObject.NULL);
          //          jsonObject22.put("pin", JSONObject.NULL);
                    jsonObject.put("user", jsonObject22);

                    LoginManager.getInstance().logOut();

                    signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithFacebook, "facebook");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //             Log.d("jkjdhkada", "Hi\n"+object.toString());
            }
        });

        Bundle parameters = new Bundle();

        parameters.putString("fields", "id, name, email, picture.width(120).height(120)");
        request.setParameters(parameters);

        request.executeAsync();
    }

    private void fingerPrintLogin() {

        // If you’ve set your app’s minSdkVersion to anything lower than 23, then you’ll need to verify that the device is running Marshmallow
        // or higher before executing any fingerprint-related code
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            //Check whether the device has a fingerprint sensor//
            if (!fingerprintManager.isHardwareDetected()) {
                // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                //           textView.setText("Your device doesn't support fingerprint authentication");
            }
            //Check whether the user has granted your app the USE_FINGERPRINT permission//
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                // If your app doesn't have this permission, then display the following text//
                //           textView.setText("Please enable the fingerprint permission");
            }

            //Check that the user has registered at least one fingerprint//
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                // If the user hasn’t configured any fingerprints, then display the following message//
                //           textView.setText("No fingerprint configured. Please register at least one fingerprint in your device's Settings");
            }

            //Check that the lockscreen is secured//
            if (!keyguardManager.isKeyguardSecure()) {
                // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                //           textView.setText("Please enable lockscreen security in your device's Settings");
            } else {
                try {
                    generateKey();
                } catch (FingerprintException e) {
                    e.printStackTrace();
                }

                if (initCipher()) {
                    //If the cipher is initialized successfully, then create a CryptoObject instance//
                    cryptoObject = new FingerprintManager.CryptoObject(cipher);

                    // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                    // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                    FingerprintHandler helper = new FingerprintHandler(this);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }
        }
    }


    //Create the generateKey method that we’ll use to gain access to the Android keystore and generate the encryption key//

    @TargetApi(Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            keyGenerator.init(new
                    //Specify the operation(s) this key can be used for//
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                    //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            //Generate the key//
            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    @TargetApi(Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //Return true if the cipher has been initialized successfully//
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            //Return false if cipher initialization failed//
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private class FingerprintException extends Exception {
        public FingerprintException(Exception e) {
            super(e);
        }
    }


    public void signInUsingFacebookGoogle(final JSONObject jsonObject, String url, final String loginType) {

        Log.d("resp_login_json", jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_login", response.toString());

                Universal_Var_Cls.isSignIn = false;
                try {
                    if(response.has("data") && response.getString("data").length() > 0){

                        editor.putString(Universal_Var_Cls.loginType, loginType);
                        editor.putString(Universal_Var_Cls.loginStr, jsonObject.toString());
                        editor.commit();

                        try {
                            String token = "Token " + response.getString("data");
                            editor.putString(Universal_Var_Cls.toKen, token);
                            editor.putString(Universal_Var_Cls.imgUrl, jsonObject.getJSONObject("user").getString("image"));
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (Universal_Var_Cls.statusLogin == 1) {

 //                           Universal_Var_Cls.statusLogin = 0;
                            Intent intent = new Intent(SignInUpActivity.this, CartActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            startActivity(intent);
                            finish();
               //             startActivity(new Intent(SignInUpActivity.this, CartActivity.class));
                        } else if (Universal_Var_Cls.statusLogin == 2) {

 //                           Universal_Var_Cls.statusLogin = 0;
                            startActivity(new Intent(SignInUpActivity.this, LiquorActivity.class));
                        } else {
                            Intent intent = new Intent(SignInUpActivity.this, MyBarActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            startActivity(intent);
                            finish();
      //                      startActivity(new Intent(SignInUpActivity.this, MyBarActivity.class));
                        }
                    }else {
                        Toast.makeText(SignInUpActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("resp_login_e", error.toString());
                new FingerLoginConsume(SignInUpActivity.this).fingerPrintLogin();

                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(SignInUpActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }


    public void signInWithFacebook(String loginStr) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(loginStr);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.signInWithFacebook, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    String token = response.getString("data");
                    editor.putString(Universal_Var_Cls.toKen, token);
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(SignInUpActivity.this, MyBarActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(SignInUpActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    public void signInWithGoogle(String loginStr) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(loginStr);
        } catch (JSONException err) {
            Log.d("Error", err.toString());
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.signInWithGoogle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String token = response.getString("data");
                    editor.putString(Universal_Var_Cls.toKen, token);
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(SignInUpActivity.this, MyBarActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(SignInUpActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void signInUsingGoogle(final JSONObject jsonObject) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Universal_Var_Cls.signInWithGoogle, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                editor.putString(Universal_Var_Cls.loginType, "google");
                editor.putString(Universal_Var_Cls.loginStr, jsonObject.toString());
                editor.commit();

                try {
                    String token = response.getString("data");
                    editor.putString(Universal_Var_Cls.toKen, token);
                    editor.putString(Universal_Var_Cls.imgUrl, jsonObject.getJSONObject("user").getString("image"));
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(SignInUpActivity.this, MyBarActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyErrorCls.volleyErr(error, context);
            }
        });
        RequestQueue queue = Volley.newRequestQueue(SignInUpActivity.this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
