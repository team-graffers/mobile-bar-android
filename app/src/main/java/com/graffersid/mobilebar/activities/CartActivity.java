package com.graffersid.mobilebar.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.CartAdapter;
import com.graffersid.mobilebar.classes.CartHelper;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.CartBottle;
import com.graffersid.mobilebar.model_cls.my_bar.Datum;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    SQLiteDatabase database;
    CartHelper helper;
    Context context;
    ImageButton btnBack;
    TextView redeemPointsAvail, validity, taxesTxt, discountTxt, titleToolBar, txtExtAmt;
    public static TextView subTotal, totalMl, taXes, disCount, totalgrand;
    CheckBox redeem;
    private TextView promoCode;
    Button checkoutCartBtn;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private DisplayMetrics displayMetrics;
    private int height, width;
    private Spinner spinnerExtendValidity;
    private List<Datum> list;
    List<String> listSpin;
    public static double price;
    private static double redeemPointRedeemed, finalAmt;
    public static int idPromo, discountPromo, qty, noOfBottles;
    private List<Datum> listPromoCode;
    private WindowManager.LayoutParams lWindowParams;
    List<CartBottle> listCart;
    public static LinearLayout noCartItem;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        context = CartActivity.this;
        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        spinnerExtendValidity = (Spinner) findViewById(R.id.extend_validity_spinner);

        idPromo = 0;
        discountPromo = 0;

        createSpinnerList();
        getRedeemPointsAndTax();

        qty = 0;
        price = 0;
        noOfBottles = 0;

        list = new ArrayList<>();
        listPromoCode = new ArrayList<>();

        helper = new CartHelper(context);
        database = helper.getWritableDatabase();
        listCart = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.cart_recycler_vw);

        displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        height = Math.round(displayMetrics.heightPixels/* / displayMetrics.density*/);
        width = Math.round(displayMetrics.widthPixels/* / displayMetrics.density*/);

        ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
        int hh = height*40 / 100;
        params.height = hh;
        recyclerView.setLayoutParams(params);
        /*RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout_below_recycler_vw);
        ViewGroup.LayoutParams paramsR = relativeLayout.getLayoutParams();
        hh = height - hh;
        paramsR.height = hh;
        relativeLayout.setLayoutParams(paramsR);*/

        titleToolBar = (TextView) findViewById(R.id.toolbar_title_cart);

        redeemPointsAvail = (TextView) findViewById(R.id.redeem_points_available);
        validity = (TextView) findViewById(R.id.validity);
        subTotal = (TextView) findViewById(R.id.sub_total);
        totalMl = (TextView) findViewById(R.id.total_ml);
        taxesTxt = (TextView) findViewById(R.id.taxes_txt);
        discountTxt = (TextView) findViewById(R.id.discount_txt);
        taXes = (TextView) findViewById(R.id.taxes);
        disCount = (TextView) findViewById(R.id.discount);
        totalgrand = (TextView) findViewById(R.id.grand_total);
        txtExtAmt = (TextView) findViewById(R.id.ext_amt);

        redeem = (CheckBox) findViewById(R.id.redeem_points);
        promoCode = (TextView) findViewById(R.id.promo_code);

        checkoutCartBtn = (Button) findViewById(R.id.bottle_checkout_cart);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnBack = (ImageButton) findViewById(R.id.back_btn_cart);
        noCartItem = (LinearLayout) findViewById(R.id.no_item_in_cart_layout);


        spinnerExtendValidity.setOnItemSelectedListener(this);
        /*createSpinnerList();
        getRedeemPointsAndTax();*/
        getPromoCode();

        prepareCartList();
    }

    private void prepareCartList() {

        CartBottle cartBottle;
        if (Universal_Var_Cls.isRenew){

            /*Universal_Var_Cls.isRenew = false;*/

            titleToolBar.setText("RENEW");

            cartBottle = new CartBottle();
            cartBottle.setId(Universal_Var_Cls.bottleId);
            cartBottle.setQtyBottle(1);
            cartBottle.setName(Universal_Var_Cls.nameBottleRenew);
            cartBottle.setCategory(Universal_Var_Cls.brandNameBottleRenew);
            cartBottle.setImage(Universal_Var_Cls.imgBottleRenew);
            cartBottle.setQtyLiqour((int) Universal_Var_Cls.qtyLiqourBottleRenew);
            cartBottle.setPrice(Universal_Var_Cls.priceBottleRenew);
            listCart.add(cartBottle);

            qty += Universal_Var_Cls.qtyLiqourBottleRenew;
            price += 1 * Universal_Var_Cls.priceBottleRenew;
            noOfBottles += 1;
            calculateFinalPrice();

            CartAdapter adapter = new CartAdapter(context, listCart);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }else {

            titleToolBar.setText("CART");

            Cursor cursor = database.query(CartHelper.TABLE_CART, null, null, null, null, null, null, null);


            if (cursor != null) {
                while (cursor.moveToNext()) {
                    cartBottle = new CartBottle();
                    cartBottle.setId(cursor.getInt(0));
                    cartBottle.setQtyBottle(cursor.getInt(1));
                    cartBottle.setName(cursor.getString(2));
                    cartBottle.setCategory(cursor.getString(3));
                    cartBottle.setImage(cursor.getString(4));
                    cartBottle.setQtyLiqour(cursor.getInt(5));
                    cartBottle.setPrice(cursor.getDouble(6));
                    listCart.add(cartBottle);

                    qty += cursor.getInt(5) * cursor.getInt(1);
                    price += cursor.getDouble(6) * cursor.getInt(1);
                    noOfBottles += cursor.getInt(1);
                }

                calculateFinalPrice();
            }

            CartAdapter adapter = new CartAdapter(context, listCart);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        checkoutCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONArray jsonArray = new JSONArray();

                JSONObject jsonObjectArray;
                for (int i=0; i<listCart.size(); i++){
                    for (int j=0; j<listCart.get(i).getQtyBottle(); j++){

                        jsonObjectArray = new JSONObject();

                        try {
                            jsonObjectArray.put("bottle_id", ""+listCart.get(i).getId());
                            if (Universal_Var_Cls.isRenew){
                                jsonObjectArray.put("is_renew", "False");
                            }else {
                                jsonObjectArray.put("is_renew", "False");
                            }
                            jsonObjectArray.put("totol_ml", ""+listCart.get(i).getQtyLiqour());
                            jsonObjectArray.put("purchased_price", ""+listCart.get(i).getPrice());

                            String createdDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                            jsonObjectArray.put("created_date_time", createdDateTime);

                            if (spinnerExtendValidity.getSelectedItemPosition()-1 >=0) {
                                jsonObjectArray.put("extend_id", list.get(spinnerExtendValidity.getSelectedItemPosition() - 1).getId());
                            }else {
                                jsonObjectArray.put("extend_id", 0);
                            }


                            int posExt = spinnerExtendValidity.getSelectedItemPosition()-1;
                            float priceExt = 0.0f;
                            double priceBottle = listCart.get(i).getPrice();

                            if (list.size() > posExt && posExt >= 0) {
                                priceExt = list.get(posExt).getPrice();
                            }

                            double finalPriceBottle = priceBottle - priceBottle*discountPromo/100;

                            //                  finalPriceBottle = finalPriceBottle + finalPriceBottle*priceExt/100;
                            finalPriceBottle = finalPriceBottle + finalPriceBottle*Universal_Var_Cls.taxDefault/100;

                            jsonObjectArray.put("final_price", finalPriceBottle);

                            jsonArray.put(jsonObjectArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (i==0 && j==0){
                            Log.d("json_bottle_purchase", jsonObjectArray.toString());
                        }
                    }
                }





                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("final_price", finalAmt);
                    jsonObject.put("promo_id", idPromo);
                    if (spinnerExtendValidity.getSelectedItemPosition()-1 >=0) {
                        jsonObject.put("extend_id", list.get(spinnerExtendValidity.getSelectedItemPosition() - 1).getId());
                    }else {
                        jsonObject.put("extend_id", 0);
                    }
                    jsonObject.put("tax", Universal_Var_Cls.taxDefault);
                    jsonObject.put("point_reddem", (Math.floor(redeemPointRedeemed*(1/Universal_Var_Cls.redeemPointFactor))));
                    jsonObject.put("payment_mode", 1);
                    jsonObject.put("bottles", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }



                Log.d("json_checkOut", jsonObject.toString());
                Universal_Var_Cls.isExtend = false;
                Intent intent = new Intent(CartActivity.this, PaymentActivity.class);
                intent.putExtra("jsonBottlePurchase", jsonObject.toString());
                startActivity(intent);

                //           bottlePurchase();
            }
        });
        applyPromoCodeClickEvent();

        redeem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                calculateFinalPrice();
            }
        });

        if (listCart.size() == 0){
            noCartItem.setVisibility(View.VISIBLE);
        }else {
            noCartItem.setVisibility(View.GONE);
        }
    }

    private void applyPromoCodeClickEvent() {

        promoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialogPromoCode = new Dialog(CartActivity.this);
                dialogPromoCode.setContentView(R.layout.dialog_apply_promo_code);
                lWindowParams = new WindowManager.LayoutParams();
                lWindowParams.copyFrom(dialogPromoCode.getWindow().getAttributes());
                lWindowParams.width = WindowManager.LayoutParams.FILL_PARENT; // this is where the magic happens
                lWindowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialogPromoCode.setCancelable(false);
                dialogPromoCode.show();
                dialogPromoCode.getWindow().setAttributes(lWindowParams);

                ImageButton closePromoDialog = (ImageButton) dialogPromoCode.findViewById(R.id.img_btn_close_apply_promo);
                final EditText promoCodeTxtDialog = (EditText) dialogPromoCode.findViewById(R.id.promo_code_dialog);
                Button btnApplyPromoCode = (Button) dialogPromoCode.findViewById(R.id.btn_apply_promo_dialog);

                closePromoDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogPromoCode.dismiss();
                    }
                });
                btnApplyPromoCode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (promoCodeTxtDialog.getText() != null){
                            String promoTxt = promoCodeTxtDialog.getText().toString();
                            boolean boolPromo = false;
                            if (listPromoCode.size() > 0) {
                                for (int i=0; i<listPromoCode.size(); i++){
                                    if (promoTxt.equals(listPromoCode.get(i).getName())){

                                        promoCode.setText(promoTxt);

                                        boolPromo = true;

                                        idPromo = listPromoCode.get(i).getId();
                                        discountPromo = listPromoCode.get(i).getDiscount();
                                        discountTxt.setText("Discount("+discountPromo+"%)");
                                        calculateFinalPrice();
                                        break;
                                    }
                                }
                            }
                            if (boolPromo){
                                dialogPromoCode.dismiss();
                            }else {
                                promoCodeTxtDialog.setError("Invalid Promo Code");
                            }
                        }
                    }
                });
            }
        });
    }

    public void calculateFinalPrice() {

        finalAmt = price;
        double discountAmt = finalAmt * discountPromo/100;

        int posExt = spinnerExtendValidity.getSelectedItemPosition()-1;
        int monthExt = 0;
        double priceExt = 0.0;
        if (list.size() > posExt && posExt >= 0){

            priceExt = list.get(posExt).getPrice();

            if (list.get(posExt).getDays()/365 > 0){
                monthExt = (list.get(posExt).getDays()/365)*12;
                monthExt += (list.get(posExt).getDays()%365)/30;
            }else {
                monthExt = list.get(posExt).getDays() / 30;
            }
        }

        monthExt += 1;
        validity.setText(""+monthExt+" Month");

        double amtExt = finalAmt*priceExt/100;

        finalAmt += finalAmt*priceExt/100;

        double taxFinal = finalAmt*Universal_Var_Cls.taxDefault/100;
        finalAmt += finalAmt * Universal_Var_Cls.taxDefault/100;

        finalAmt -= discountAmt;

        int redeemPointFinal = 0;
        if (redeem.isChecked()){

            Log.d("redeem_factor", ""+Universal_Var_Cls.redeemPointFactor);
            redeemPointFinal = (int) Math.floor(Universal_Var_Cls.redeemPoint*Universal_Var_Cls.redeemPointFactor);
        }

        if (finalAmt > redeemPointFinal){
            finalAmt -= redeemPointFinal;
            redeemPointRedeemed = redeemPointFinal;
        }else {
            redeemPointRedeemed = finalAmt;
            finalAmt = 0.0;
        }

        subTotal.setText("S$ " + String.format("%.2f", price));
        txtExtAmt.setText("Extend Validity(S$ " + String.format("%.2f", amtExt)+")");
        totalMl.setText("" + qty + "ml");

        disCount.setText("S$ " + String.format("%.2f", discountAmt));

        taXes.setText("S$ " + String.format("%.2f", taxFinal));

        totalgrand.setText("S$ " + String.format("%.2f", finalAmt));
    }

    private void getPromoCode() {

        String url = Universal_Var_Cls.applyPromocode;
        JSONObject jsonObject = new JSONObject();
        try {
            Calendar cal = Calendar.getInstance();
            Date date = new Date();
            date = cal.getTime();
            String datetttt = new SimpleDateFormat("yyyy-MM-dd").format(date);
            jsonObject.put("created_date_time", datetttt);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("response_list_promo", jsonObject.toString());

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_list_promo", response.toString());
                listPromoCode = new Gson().fromJson(response.toString(), MyBarModelCls.class).getData();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                /*redeemPointsAvail.setText("Redeem Points(0 Points)");
                taxesTxt.setText("Taxes");*/
                Log.d("response_list_promo_e", error.toString()/*+"   "+error.networkResponse.statusCode*/);
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void getRedeemPointsAndTax() {

        String url = Universal_Var_Cls.customerReedemPointAndTax;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("response_list_redeem", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.getJSONArray("data").get(0).toString());

                    Universal_Var_Cls.redeemPoint = jsonObject.getInt("point");
                    Universal_Var_Cls.taxDefault = jsonObject.getDouble("tax_value");
                    Universal_Var_Cls.redeemPointFactor = jsonObject.getDouble("redeem_factor");

                    /*Universal_Var_Cls.taxDefault = response.getJSONObject("data").getDouble("default_tax");
                    Universal_Var_Cls.redeemPointFactor = response.getJSONObject("data").getDouble("point_redeem_factor");
                    Universal_Var_Cls.redeemPoint = response.getJSONObject("data").getInt("point_redeem_value");
                    Universal_Var_Cls.publishableKey = response.getJSONObject("data").getString("payment_publishable_key");
                    Universal_Var_Cls.secretKey = response.getJSONObject("data").getString("admin_payment_key");*/
                    redeemPointsAvail.setText("Redeem Points("+Universal_Var_Cls.redeemPoint+" Points)");
                    taxesTxt.setText("Taxes("+Universal_Var_Cls.taxDefault+"%)");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                redeemPointsAvail.setText("Redeem Points(0 Points)");
                taxesTxt.setText("Taxes");
                Log.d("response_list_redeem", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void createSpinnerList() {

        String url = Universal_Var_Cls.extendValidityList;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //             Log.d("response_list", response.toString());
                list = new Gson().fromJson(response.toString(), MyBarModelCls.class).getData();

                listSpin = new ArrayList<>();

                Comparator<Datum> compareById = new Comparator<Datum>() {
                    @Override
                    public int compare(Datum o1, Datum o2) {
                        return o1.getDays().compareTo(o2.getDays());
                    }
                };

                Collections.sort(list, compareById);

                /*for (int i=0; i<list.size(); i++){
                    for (int j=i+1; j<list.size(); j++){
                        if (list.get(i).getDays() > list.get(j).getDays()){
                            Datum datum = new Datum();
                            datum = list.get(i);
                            list.set(i, list.get(j));
                            list.set(j, datum);
                        }
                    }
                }*/

                for (int i = 0; i < list.size(); i++) {
                    listSpin.add(list.get(i).getName());
                }

                listSpin.add(0, "Select");

                createSpinner();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response_list", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        });

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    private void createSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, listSpin) {
            @Override
            public boolean isEnabled(int position) {

                /*if (position == 0) {
                    return false;
                } else {
                    return true;
                }*/
                return true;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View view = super.getDropDownView(position, convertView, parent);

                TextView txtView = (TextView) view;

                if (position == 0) {
                    txtView.setTextColor(Color.GRAY);
                }
   //             txtView.setTextColor(Color.parseColor("#9C9C9C"));
                return view;
            }
        };


        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerExtendValidity.setAdapter(adapter);

 //       spinnerExtendValidity.setSelection(1);
 //       int pos = spinnerExtendValidity.getSelectedItemPosition();
    }

    private void bottlePurchase() {

        Universal_Var_Cls.loader(context);
        Universal_Var_Cls.dialog.show();

        JSONArray jsonArray = new JSONArray();

        JSONObject jsonObjectArray;
        for (int i=0; i<listCart.size(); i++){
            for (int j=0; j<listCart.get(i).getQtyBottle(); j++){

                jsonObjectArray = new JSONObject();

                try {
                    jsonObjectArray.put("bottle_id", ""+listCart.get(i).getId());
                    if (Universal_Var_Cls.isRenew){
                        jsonObjectArray.put("is_renew", "False");
                    }else {
                        jsonObjectArray.put("is_renew", "False");
                    }
                    jsonObjectArray.put("totol_ml", ""+listCart.get(i).getQtyLiqour());
                    jsonObjectArray.put("purchased_price", ""+listCart.get(i).getPrice());

                    String createdDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    jsonObjectArray.put("created_date_time", createdDateTime);

                    if (spinnerExtendValidity.getSelectedItemPosition()-1 >=0) {
                        jsonObjectArray.put("extend_id", list.get(spinnerExtendValidity.getSelectedItemPosition() - 1).getId());
                    }else {
                        jsonObjectArray.put("extend_id", 0);
                    }


                    int posExt = spinnerExtendValidity.getSelectedItemPosition()-1;
                    float priceExt = 0.0f;
                    double priceBottle = listCart.get(i).getPrice();

                    if (list.size() > posExt && posExt >= 0) {
                        priceExt = list.get(posExt).getPrice();
                    }

                    double finalPriceBottle = priceBottle - priceBottle*discountPromo/100;

  //                  finalPriceBottle = finalPriceBottle + finalPriceBottle*priceExt/100;
                    finalPriceBottle = finalPriceBottle + finalPriceBottle*Universal_Var_Cls.taxDefault/100;

                    jsonObjectArray.put("final_price", finalPriceBottle);

                    jsonArray.put(jsonObjectArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (i==0 && j==0){
                    Log.d("json_bottle_purchase", jsonObjectArray.toString());
                }
            }
        }





        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("final_price", finalAmt);
            jsonObject.put("promo_id", idPromo);
            if (spinnerExtendValidity.getSelectedItemPosition()-1 >=0) {
                jsonObject.put("extend_id", list.get(spinnerExtendValidity.getSelectedItemPosition() - 1).getId());
            }else {
                jsonObject.put("extend_id", 0);
            }
            jsonObject.put("tax", Universal_Var_Cls.taxDefault);
            jsonObject.put("point_reddem", redeemPointRedeemed);
            jsonObject.put("payment_mode", 1);
            jsonObject.put("bottles", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        Log.d("json_checkOut", jsonObject.toString());

        String url = Universal_Var_Cls.bottlePurchase;

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.isRenew = !Universal_Var_Cls.isRenew;

                Universal_Var_Cls.dialog.dismiss();
                Log.d("resp_bottle_save_s", response.toString());
                database.delete(CartHelper.TABLE_CART, null, null);
                startActivity(new Intent(CartActivity.this, MyBarActivity.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Universal_Var_Cls.dialog.dismiss();
                Log.d("resp_bottle_save_e", error.toString()+"  ");
                VolleyErrorCls.volleyErr(error, context);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);

        queue.add(request);
    }

    @Override
    public void onBackPressed() {

        if (Universal_Var_Cls.isRenew){
            Universal_Var_Cls.isRenew = !Universal_Var_Cls.isRenew;
        }
        if (Universal_Var_Cls.statusLogin == 2) {

            //                           Universal_Var_Cls.statusLogin = 0;
            startActivity(new Intent(CartActivity.this, LiquorActivity.class));
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        calculateFinalPrice();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
