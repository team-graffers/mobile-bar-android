package com.graffersid.mobilebar.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.CurrentLocation;
import com.graffersid.mobilebar.classes.PermissionManager;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;

import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.HashMap;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {

    Context context;

    private View mContentView;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static final String[] RUNTIME_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.USE_FINGERPRINT
    };
    private HashMap<String, String> headers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

 //       new CurrentLocation(this).lattLong();

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        Universal_Var_Cls.jsonInitialize();
        Universal_Var_Cls.listBottle = new ArrayList<>();
        Universal_Var_Cls.filterCategoryIdList = new ArrayList<>();
        Universal_Var_Cls.filterBarIdList = new ArrayList<>();

        context = SplashActivity.this;
        PermissionManager permissionManager = new PermissionManager();

        if (permissionManager.hasPermissions(context, RUNTIME_PERMISSIONS)) {
            new CurrentLocation(context).lattLong();
        } else {
            ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, 1);
        }

        Universal_Var_Cls.lat = 0.0;
        Universal_Var_Cls.lng = 0.0;
        new CurrentLocation(context).lattLong();

        /*editor.putString(Universal_Var_Cls.toKen, "ed4b3fd98bb50135e1f5a1a17b5455f4bf06c63e");
        editor.commit();*/

        if (preferences.getString(Universal_Var_Cls.toKen, null) != null) {
            headers = new HashMap<>();
            headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
            headers.put("Content-Type", "application/json");
            startActivity(new Intent(SplashActivity.this, MyBarActivity.class));
        }

        mContentView = findViewById(R.id.fullscreen_content);
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        /*mContentView.setSystemUiVisibility(
                 View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);*/

        findViewById(R.id.splash_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottleListActivity();
            }
        });
    }

    private void bottleListActivity() {

        startActivity(new Intent(SplashActivity.this, LiquorActivity.class));

        /*Universal_Var_Cls.filterCategoryIdList = new ArrayList<>();
        Universal_Var_Cls.filterBarIdList = new ArrayList<>();

        new Universal_Var_Cls();

        findViewById(R.id.splash_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                findViewById(R.id.splash_btn).setEnabled(false);
                String url = Universal_Var_Cls.bottleListUrl;

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, Universal_Var_Cls.jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        findViewById(R.id.splash_btn).setEnabled(true);

                        Universal_Var_Cls.liqourBottleListCls = new Gson().fromJson(response.toString(), LiquorBottleListCls.class);

                        Universal_Var_Cls.countLiqourBottle = Universal_Var_Cls.liqourBottleListCls.getCount();
                        startActivity(new Intent(SplashActivity.this, LiquorActivity.class));
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        findViewById(R.id.splash_btn).setEnabled(true);
                    }
                });

                RequestQueue queue = Volley.newRequestQueue(context);
                int timeOut = 15000;
                DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                request.setRetryPolicy(retryPolicy);
                queue.add(request);
            }
        });

        int statusBarHeight = getStatusBarHeight();
        Log.d("height_status_bar", "Status Bar: "+statusBarHeight);*/
    }

    PulsatorLayout pulsatorLayout;

    @Override
    protected void onStart() {
        super.onStart();
        pulsatorLayout = (PulsatorLayout) findViewById(R.id.pulsator);
        pulsatorLayout.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //      pulsatorLayout.stop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 1: {
                for (int index = 0; index < permissions.length; index++) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {

                        /*
                         * If the user turned down the permission request in the past and chose the
                         * Don't ask again option in the permission request system dialog.
                         */
                        if (!ActivityCompat
                                .shouldShowRequestPermissionRationale(this, permissions[index])) {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                            + " not granted. "
                                            + "Please go to settings and turn on for sample app",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Required permission " + permissions[index]
                                    + " not granted", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                Log.d("permission_check", "After permission granted");
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
