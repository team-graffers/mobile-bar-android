package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.profile.ProfileModelCls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView nameUser, emailUser, phoneUser, pinUser;
    EditText nameEdit, emailEdit, phoneEdit, pinEdit;
    View phoneDot;
    Button btnEdit, btnUpdate, verifyMobile;
    ImageView imgProfile;
    ImageButton btnBack;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private HashMap<String, String> headers;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        preferences = getSharedPreferences(Universal_Var_Cls.preferences, MODE_PRIVATE);
        editor = preferences.edit();

        context = ProfileActivity.this;

        Universal_Var_Cls.loader(context);
        Universal_Var_Cls.dialog.show();

        headers = new HashMap<>();
        headers.put("Authorization", preferences.getString(Universal_Var_Cls.toKen, null));
        headers.put("Content-Type", "application/json");

        initialize();
        onClickMethod();

        profileDetailMethod();
    }

    private void initialize() {

        btnBack = (ImageButton) findViewById(R.id.back_btn_profile);

        imgProfile = (ImageView) findViewById(R.id.img_profile);
        nameUser = (TextView) findViewById(R.id.name_user);
        emailUser = (TextView) findViewById(R.id.email_id);
        phoneUser = (TextView) findViewById(R.id.phone_number);
        pinUser = (TextView) findViewById(R.id.pin_user);
        nameEdit = (EditText) findViewById(R.id.name_user_edit);
        emailEdit = (EditText) findViewById(R.id.email_id_edit);
        phoneEdit = (EditText) findViewById(R.id.phone_number_edit);
        pinEdit = (EditText) findViewById(R.id.pin_edit);

        phoneDot = (View) findViewById(R.id.phone_dot);

        btnEdit = (Button) findViewById(R.id.edit_profile);
        btnUpdate = (Button) findViewById(R.id.update_profile);
        verifyMobile = (Button) findViewById(R.id.btn_verify_mobile);


        nameEdit.setVisibility(View.GONE);
        emailEdit.setVisibility(View.GONE);
        phoneEdit.setVisibility(View.GONE);
        pinEdit.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);

        pinUser.setVisibility(View.GONE);
    }

    private void onClickMethod() {

        btnBack.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        verifyMobile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back_btn_profile:
                onBackPressed();
                break;
            case R.id.edit_profile:
                editProfile();
                break;
            case R.id.update_profile:

                String name = "";
                String email = "";
                String phone = "";
                if (nameEdit.getText().toString().length() > 0) {
                    if (emailEdit.getText().toString().length() > 0) {
                        if (Universal_Var_Cls.isValidate(emailEdit.getText().toString())) {

                            if (phoneEdit.getText().toString().length() > 0) {
                                phone = phoneEdit.getText().toString();
                            }
                            name = nameEdit.getText().toString();
                            email = emailEdit.getText().toString();

                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("name", name);
                                jsonObject.put("email", email);
                                jsonObject.put("phone", phone);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            updateProfile(jsonObject, name, email, phone);
                        } else emailEdit.setError("Enter valid email");
                    } else emailEdit.setError("Enter email");
                } else nameEdit.setError("Enter name");
                break;
            case R.id.btn_verify_mobile:
                verifyNumber();
        }
    }

    private void editProfile() {
        nameUser.setVisibility(View.GONE);
        emailUser.setVisibility(View.GONE);
        phoneUser.setVisibility(View.GONE);
        pinUser.setVisibility(View.GONE);
        btnEdit.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.VISIBLE);

        nameEdit.setVisibility(View.VISIBLE);
        emailEdit.setVisibility(View.VISIBLE);
        phoneEdit.setVisibility(View.GONE);
        pinEdit.setVisibility(View.GONE);
    }

    private void updateProfile(JSONObject jsonObject, final String name, final String email, final String phone) {

        Universal_Var_Cls.dialog.show();
        String url = Universal_Var_Cls.editProfile;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Universal_Var_Cls.dialog.dismiss();

                Log.d("profile_resp_success", response.toString());

                nameUser.setVisibility(View.VISIBLE);
                emailUser.setVisibility(View.VISIBLE);
                phoneUser.setVisibility(View.VISIBLE);
                pinUser.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
                btnUpdate.setVisibility(View.GONE);

                nameEdit.setVisibility(View.GONE);
                emailEdit.setVisibility(View.GONE);
                phoneEdit.setVisibility(View.GONE);
                pinEdit.setVisibility(View.GONE);


                nameUser.setText(name);
                nameEdit.setText(name);
                emailUser.setText(email);
                emailEdit.setText(email);

                if (phone.length() > 0) {
                    phoneUser.setText(phone);
                    phoneEdit.setText(phone);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("profile_resp_error", error.toString());
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }

    private void verifyNumber() {

        Universal_Var_Cls.isMobileVerification = true;
        Intent intent = new Intent(ProfileActivity.this, MobileOtpVerifyActivity.class);
        startActivityForResult(intent, 1022);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1022 && resultCode == RESULT_OK) {

            phoneUser.setText(data.getStringExtra("mobile"));
        }
    }

    private void profileDetailMethod() {

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, Universal_Var_Cls.profileDetail, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Universal_Var_Cls.dialog.dismiss();

                ProfileModelCls profileModelCls = null;
                try {
                    profileModelCls = new Gson().fromJson(response.getJSONObject(0).toString(), ProfileModelCls.class);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jsonObject = response.getJSONObject(0);

                    try {
                        if (jsonObject.getString("email") != null) {
                            emailUser.setText(jsonObject.getString("email"));
                            emailEdit.setText(jsonObject.getString("email"));
                        }

                        if (jsonObject.getString("name") != null) {
                            nameUser.setText(jsonObject.getString("name"));
                            nameEdit.setText(jsonObject.getString("name"));
                        }
                        if (jsonObject.getBoolean("verified") == false) {
                            phoneDot.setVisibility(View.VISIBLE);
                            phoneDot.setBackgroundResource(R.drawable.phone_dot_red);
                            verifyMobile.setText("Verify");
                        } else {
                            phoneDot.setVisibility(View.VISIBLE);
                            phoneDot.setBackgroundResource(R.drawable.phone_dot_green);
                            verifyMobile.setText("Change");
                        }


                        String img = jsonObject.getString("image");
                        String imggg = "";

                        if (img != null && img.length() > 0) {
                            Picasso.with(context).load(img).placeholder(R.drawable.default_img).error(R.drawable.default_img).into(imgProfile);
                        }

                        if (profileModelCls.getPhone() != null) {

                            phoneUser.setText("" + profileModelCls.getPhone());
                            phoneEdit.setText("" + profileModelCls.getPhone());
                        }

                        if (profileModelCls.getPin() != null) {
                            pinUser.setText(profileModelCls.getPin());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("profile_resp_success", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("profile_resp_error", error.toString());
                Universal_Var_Cls.dialog.dismiss();
                VolleyErrorCls.volleyErr(error, context);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
