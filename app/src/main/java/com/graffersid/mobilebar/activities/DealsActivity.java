package com.graffersid.mobilebar.activities;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.adapter.DealsAdapter;
import com.graffersid.mobilebar.classes.Universal_Var_Cls;
import com.graffersid.mobilebar.classes.VolleyErrorCls;
import com.graffersid.mobilebar.model_cls.my_bar.MyBarModelCls;
import com.graffersid.mobilebar.model_cls.my_bar.Result;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DealsActivity extends AppCompatActivity {

    private boolean isScrolling = false, isPaginate = false;
    private int currentItems, totalItems, scrollOutItems;
    GridLayoutManager layoutManager;
    private static String nextPage, previousPage;

    ImageButton btnBack;
    RecyclerView recyclerView;
    private List<Result> listDeals;
    DealsAdapter adapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals);

        context = DealsActivity.this;
        recyclerView = (RecyclerView) findViewById(R.id.recycler_vw_deals);
        recyclerView.setPadding(0, 15, 0, 30);
        btnBack = (ImageButton) findViewById(R.id.back_btn_deals);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        listDeals = new ArrayList<>();
        adapter = new DealsAdapter(this, listDeals);

        layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Universal_Var_Cls.loader(this);
        Universal_Var_Cls.dialog.show();

        dealsList(Universal_Var_Cls.dealsShown);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {

                    isScrolling = false;
                    if (nextPage != null) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isPaginate = true;
                                dealsList(nextPage);
                            }
                        }, 100);
                    }
                }
            }
        });
    }

    private void dealsList(String url) {

        if (Universal_Var_Cls.dialog.isShowing()){

        }else {
            Universal_Var_Cls.dialog.show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("resp_deals", response.toString());
                Universal_Var_Cls.dialog.dismiss();

                MyBarModelCls myBarModelCls = new Gson().fromJson(response.toString(), MyBarModelCls.class);

                if (myBarModelCls.getResults() != null && !myBarModelCls.getResults().isEmpty()){

                    listDeals.addAll(myBarModelCls.getResults());
                    adapter.notifyDataSetChanged();
                    if (myBarModelCls.getNext() != null){

                        String str = myBarModelCls.getNext();

                        if (str.contains("localhost")){
                            str = str.replace("localhost", Universal_Var_Cls.baseDomain);
                        }
                        nextPage = str;
                    }else {
                        nextPage = null;
                    }
                }

  //              adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Universal_Var_Cls.dialog.dismiss();
                Log.d("resp_deals", error.toString());
                VolleyErrorCls.volleyErr(error, context);
            }
        })/*{
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        }*/;

        RequestQueue queue = Volley.newRequestQueue(this);
        int timeOut = 15000;
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        queue.add(request);
    }
}
