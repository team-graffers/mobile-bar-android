package com.graffersid.mobilebar.classes;

import android.app.Dialog;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;

import com.graffersid.mobilebar.R;
import com.graffersid.mobilebar.model_cls.CartBottle;
import com.graffersid.mobilebar.model_cls.filter_category_list.FilterCategoryList;
import com.graffersid.mobilebar.model_cls.filter_category_list.ResultCategory;
import com.graffersid.mobilebar.model_cls.liquor_list.LiquorBottleListCls;
import com.graffersid.mobilebar.model_cls.liquor_list.ResultBottle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Sandy on 29/07/2019 at 01:55 PM.
 */
public class Universal_Var_Cls {

/**
 *  Universal variables
 * **/
    public static final String preferences = "preferences";
    public static final String loginStr = "loginCredentials";
    public static final String loginType = "loginTypeFbGoogle";
    public static final String toKen = "loginToken";
    public static final String imgUrl = "imageUrl";
    public static final String bottlePurchasedId = "bottlePurchasedId";
    public static final String priceExtend = "priceExtend";
    public static String nameBottleRenew = "";
    public static String imgBottleRenew = "";
    public static String mailId = null;
    public static String brandNameBottleRenew = "";
    public static String name;
    public static String category;
    public static String image;
    public static String publishableKey = "";
    public static String secretKey = "";


    public static int statusLogin = 0;
    public static int minPriceFilter = 0;
    public static int maxPriceFilter = 0;
    public static int minSelectedFilterPrice = 0;
    public static int maxSelectedFilterPrice = 0;
    public static int barId = -1;
    public static int bottleId = -1;
    public static int bottleIdPurchased = -1;
    public static int customerBottleId = -1;
    public static int statusHistory = 0;
    public static int bottleQty = 0;
    public static int counter = 0;
    public static int countLiqourBottle = 0;
    public static int pinCustomer = 0;
    public static int redeemPoint = 0;

    public static double bottlePrice = 0.0;
    public static double liqourLeft = 0.0;
    public static double lat;
    public static double lng;
    public static double priceBottleRenew = 0;
    public static double qtyLiqourBottleRenew = 0;
    public static double liqourQty = 0.0;
    public static double taxDefault = 0.0;
    public static double redeemPointFactor = 0.0;

    public static boolean statusFilter = false;
    public static boolean isRenew = false;
    public static boolean isExtGift = false;
    public static boolean isBottle = false;
    public static boolean isExtend = false;
    public static boolean isSignIn = false;
    public static boolean isSignUp = false;
    public static boolean isMobileVerification = false;

    public static JSONObject jsonObject = new JSONObject();


/**
 *   Universal list for filter
 * **/
    public static List<String> filterCategoryIdList = new ArrayList<>();
    public static List<String> filterBarIdList = new ArrayList<>();
    public static List<ResultCategory> listcategory;
    public static List<ResultCategory> listBar;
    public static List<ResultBottle> listBottle;
    public static List<CartBottle> listCart;


/**
 *  Url's
 * **/
    public final static String baseDomain = "172.104.52.17:8000";
    public final static String baseUrl = "http://172.104.52.17:8000";

    public static final String bottleListUrl = baseUrl+"/bottlelist";
    public static final String filterCategoryUrl = baseUrl+"/filtercategory";
    public static final String filterBarUrl = baseUrl+"/Bar";
    public static final String filterMinMaxPriceUrl = baseUrl+"/MinMaxPriceView";
    public static final String bottleDetailUrl = baseUrl+"/bottledetail";

    public static final String signInWithFacebook = baseUrl+"/facebook_login?isSignUp="+isSignUp;
    public static final String signInWithGoogle = baseUrl+"/google_login?isSignUp="+isSignUp;
    public static final String myBarByBottle = baseUrl+"/customerbottleView";
    public static final String myBarByBar = baseUrl+"/mobbar";
    public static final String bottlePurchase = baseUrl+"/bottlesave";
    public static final String extendValidityList = baseUrl+"/extends";
    public static final String customerReedemPointAndTax = baseUrl+"/CustomerReedemPoint";
    public static final String adminSetting = baseUrl+"/AdminSetting";
    public static final String applyPromocode = baseUrl+"/ApplyCustomerPromocode";
    public static final String dealsShown = baseUrl+"/DealShown";
    public static final String transactionHistory = baseUrl+"/TranscationHistory";
    public static final String profileDetail = baseUrl+"/CustomerProfileDetail";
    public static final String editProfile = baseUrl+"/CustomerEditProfile";
    public static final String availablePurchasedBottle = baseUrl+"/bot_avail_";
    public static final String extendBottleValidity = baseUrl+"/extendbottledate";
    public static final String bottlesAtBar = baseUrl+"/barbottlestories";
    public static final String barsForBottle = baseUrl+"/customerbottledetail/?customer_bottle_id=";
    public static final String sendingGift = baseUrl+"/SendingGift";
    public static final String verifyCustomerNo = baseUrl+"/VerifyCustomerNo";
    public static final String checkUserOTP = baseUrl+"/CheckUserOTP";
    public static final String generatePin = baseUrl+"/Generatepin";
    public static final String consumeBarBottleDetail = baseUrl+"/cartdetailbarbottle";
    public static final String orderDrinkCreateView = baseUrl+"/orderdrinkCreateView";
    public static final String generateDrinkToken = baseUrl+"/GenerateDrinkToken";
    public static final String bottleHistory = baseUrl+"/bottleHistory";
    public static final String consumptionHistory = baseUrl+"/NewOrderDrinkData";
    public static final String bottleConsumptionHistory = baseUrl+"/ConsumptionHistory?bottle_id=";
    public static final String gifted_list = baseUrl+"/Gifted_list";
    public static final String giftedDetail = baseUrl+"/abc?gift_id=";
    public static final String cancelGift = baseUrl+"/CancelGift?gift_id=";
    public static final String referCode = baseUrl+"/reffer";
    public static final String referFriend = baseUrl+"/ReFFerFriend";




    


/**
 *   Universal Instances for server data
 * **/
    public static LiquorBottleListCls liqourBottleListCls = null;
    public static FilterCategoryList filterCategoryList = null;
    public static FilterCategoryList filterBarList = null;

    public static void jsonInitialize(){
        try {
            jsonObject.put("category", new JSONArray());
            jsonObject.put("bar", new JSONArray());
            jsonObject.put("range_start", "");
            jsonObject.put("range_end", "");
            jsonObject.put("name", "");
            jsonObject.put("sort_price_down_to_up", "");
            jsonObject.put("sort_price_up_to_down", "");
            jsonObject.put("character_asscending", "");
            jsonObject.put("character_decending", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isGpsEnabled(Context context){
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    public static Dialog dialog;
    public static void loader(Context context){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_progress_layout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
    }


    public static boolean isValidate(String emailId) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (emailId.matches(emailPattern) && emailId.length() > 0) {
            return true;
        }
        return false;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
