package com.graffersid.mobilebar.classes;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.graffersid.mobilebar.activities.ConsumeLiquorActivity;
import com.graffersid.mobilebar.activities.SignInUpActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sandy on 21/08/2019 at 10:44 AM.
 */
@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    // You should use the CancellationSignal method whenever your app can no longer process user input, for example when your app goes
    // into the background. If you don’t use this method, then other apps will be unable to access the touch sensor, including the lockscreen!

    private CancellationSignal cancellationSignal;
    private Context context;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public FingerprintHandler(Context mContext) {
        context = mContext;
        preferences = context.getSharedPreferences(Universal_Var_Cls.preferences, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    //Implement the startAuth method, which is responsible for starting the fingerprint authentication process

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {

        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    //onAuthenticationError is called when a fatal error has occurred. It provides the error code and error message as its parameters
    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        //I’m going to display the results of fingerprint authentication as a series of toasts.
        //Here, I’m creating the message that’ll be displayed if an error occurs
//        Toast.makeText(context, "Authentication error\n" + errString, Toast.LENGTH_LONG).show();
        new FingerLoginConsume(context).fingerPrintLogin();
    }

    //onAuthenticationFailed is called when the fingerprint doesn’t match with any of the fingerprints registered on the device
    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(context, "Authentication failed", Toast.LENGTH_LONG).show();
    }

    //onAuthenticationHelp is called when a non-fatal error has occurred. This method provides additional information about the error,
    //so to provide the user with as much feedback as possible I’m incorporating this information into my toast
    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        Toast.makeText(context, "Authentication help\n" + helpString, Toast.LENGTH_LONG).show();
    }

    /*onAuthenticationSucceeded is called when a fingerprint has been successfully matched to one of the fingerprints stored on the user’s device*/
    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        if (Universal_Var_Cls.isSignUp){

        }else if (Universal_Var_Cls.isSignIn && preferences.getString(Universal_Var_Cls.loginStr, null) != null){

            SignInUpActivity activity = (SignInUpActivity) context;

            JSONObject jsonObject = null;
            JSONObject jsonObject22 = new JSONObject();
            try {
                jsonObject = new JSONObject(preferences.getString(Universal_Var_Cls.loginStr, null));

                jsonObject22.put("name", jsonObject.getJSONObject("user").getString("name"));
                jsonObject22.put("image", jsonObject.getJSONObject("user").getString("image"));

                new CurrentLocation(context).lattLong();

                jsonObject22.put("xcord", Universal_Var_Cls.lat);
                jsonObject22.put("ycord", Universal_Var_Cls.lng);
                jsonObject22.put("reffer_code", "");

                if (preferences.getString(Universal_Var_Cls.loginType, null).equals("facebook")){

                    jsonObject.put("user", jsonObject22);
                    activity.signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithFacebook, preferences.getString(Universal_Var_Cls.loginType, null));
                }else if (preferences.getString(Universal_Var_Cls.loginType, null).equals("google")){

                    jsonObject22.put("person_number", "");
                    jsonObject.put("user", jsonObject22);
                    activity.signInUsingFacebookGoogle(jsonObject, Universal_Var_Cls.signInWithGoogle, preferences.getString(Universal_Var_Cls.loginType, null));
                }else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }/*else if (Universal_Var_Cls.pinCustomer != 0){

            ConsumeLiquorActivity activity = (ConsumeLiquorActivity) context;
            activity.verifyCustomerPin();
        }else {
            Toast.makeText(context, "Please enter amount of liquor", Toast.LENGTH_SHORT).show();
            new FingerLoginConsume(context).fingerPrintLogin();
        }*/
    }

}