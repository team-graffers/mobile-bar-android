package com.graffersid.mobilebar.classes;

import android.content.Context;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

/**
 * Created by Sandy on 19-03-2019 at 12:30 PM.
 */
public class CurrentLocation {

    Context context;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;

    public CurrentLocation(Context context) {
        this.context = context;
    }

    public void lattLong() {

        fusedLocationProviderClient = new FusedLocationProviderClient(context);

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(2000);
        locationRequest.setInterval(4000);

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Universal_Var_Cls.lat = locationResult.getLastLocation().getLatitude();
                Universal_Var_Cls.lng = locationResult.getLastLocation().getLongitude();
            }
        }, Looper.getMainLooper());
    }
}
