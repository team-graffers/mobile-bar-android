package com.graffersid.mobilebar.classes;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Sandy Pati on 23-10-2019 at 04:47 PM.
 */
public class FabricCrashlytics extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
    }
}
